<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>DASHBOARD ELECTRA</title>
    <meta name="description" content="DASHBOARD ELECTRA 8">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <!-- <link rel="shortcut icon" href="favicon.ico">  isi icon electra nanti -->

    <link rel="stylesheet" href="{{url('dashboard/css/normalize.css')}}">
    <link rel="stylesheet" href="{{url('dashboard/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{url('dashboard/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{url('dashboard/css/themify-icons.css')}}">
    <link rel="stylesheet" href="{{url('dashboard/css/flag-icon.min.css')}}">
    <link rel="stylesheet" href="{{url('dashboard/css/cs-skin-elastic.css')}}">
    <!-- <link rel="stylesheet" href="assets/css/bootstrap-select.less"> -->
    <link rel="stylesheet" href="{{url('dashboard/scss/style.css')}}">


    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>



</head>
<body>

  @if(session()->has('message'))
  <script>alert(' {{ session()->get('message') }}')</script>
  @endif


    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">

            <div class="navbar-header">
                <p class="navbar-brand">ELECTRA 8</p>


            </div>

            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="active">
                        <a href="/home">Dashboard </a>
                    </li>
                    <h3 class="menu-title">Profile</h3>
                    <li>
                        <a href="#">EDIT DATA TIM</a>

                    </li>


                    <!-- //<h3 class="menu-title">Tambah Menu apa</h3> -->

                </ul>
            </div>
        </nav>
    </aside>



    <div id="right-panel" class="right-panel">


        <header id="header" class="header">

            <div class="header-menu">


                <div class="col-sm-12">
                    <div class="user-area dropdown float-right">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <p>Halo Team {{Auth::user()->name}}</p>
                        </a>

                        <div class="user-menu dropdown-menu">
                                <a class="nav-link" href="#"><i class="fa fa- user"></i>My Profile</a>
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                        </div>
                    </div>
                </div>
            </div>

        </header><!-- /header -->
        <!-- Header-->

        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>EDIT DATA TEAM</h1>
                    </div>
                </div>
            </div>

        </div>

        <div class="col-md-12">

          <div class="card">
              <div class="card-header">
                  <strong class="card-title">EDIT</strong>
              </div>
              <div class="card-body">


                <form method="post" action="/home/editteam/edit">
                  {{csrf_field()}}

                      <br>
                      <div class="form-group row add">
                        <label class="control-label col-sm-2">Nama Tim :</label>
                        <div class="col-sm-9">
                        @if($user->name == NULL)
                        <input id="name" type="text" class="form-control col-lg-8" name="name" placeholder="Nama Tim" value="{{ old('name') }}" required>
                        @else
                        <input id="name" type="text" class="form-control col-lg-8" name="name" placeholder="Nama Tim" value="{{$user->name}}" required>
                        @endif
                      </div></div>
                      <br>
                      <div class="form-group row add">
                        <label class="control-label col-sm-2">Email :</label>
                        <div class="col-sm-9">
                        @if($user->email == NULL)
                        <input id="email" type="text" class="form-control col-lg-8" name="email" placeholder="Email" value="{{ old('email') }}" required>
                        @else
                        <input id="email" type="text" class="form-control col-lg-8" name="email" placeholder="Email" value="{{$user->email}}" required>
                        @endif
                      </div></div>
                      <br>


                      <div class="form-group row add">
                        <label class="control-label col-sm-2">Nomor Telepon :</label>
                        <div class="col-sm-9">
                        @if($user->notelp == NULL)
                        <input id="notelp" type="text" class="form-control col-lg-8" name="notelp" placeholder="Nomor Telepon" value="{{ old('notelp') }}" required>
                        @else
                        <input id="notelp" type="text" class="form-control col-lg-8" name="notelp" placeholder="Nomor Telepon" value="{{$user->notelp}}" required>
                        @endif
                      </div></div>
                       <br>
                      <div class="form-group row add">
                        <label class="control-label col-sm-2">Asal Sekolah :</label>
                        <div class="col-sm-9">
                        @if($user->asalsekolah == NULL)
                        <input id="asalsekolah" type="text" class="form-control col-lg-8" name="asalsekolah" placeholder="Asal Sekolah" value="{{ old('asalsekolah') }}" required>
                        @else
                        <input id="asalsekolah" type="text" class="form-control col-lg-8" name="asalsekolah" placeholder="Asal Sekolah" value="{{$user->asalsekolah}}" required>
                        @endif
                      </div></div>
                      <br>
                      <div class="form-group row add">
                        <label class="control-label col-sm-2">Alamat Sekolah :</label>
                        <div class="col-sm-9">
                        @if($user->alamatsekolah == NULL)
                        <input id="alamatsekolah" type="text" class="form-control col-lg-8" name="alamatsekolah" placeholder="Alamat Sekolah" value="{{ old('alamatsekolah') }}" required>
                        @else
                        <input id="alamatsekolah" type="text" class="form-control col-lg-8" name="alamatsekolah" placeholder="Alamat Sekolah" value="{{$user->alamatsekolah}}" required>
                        @endif
                      </div></div>
                      <br>
                      <div class="form-group row add">
                        <label class="control-label col-sm-2">Nama Ketua :</label>
                        <div class="col-sm-9">
                        @if($user->namaketua == NULL)
                        <input id="namaketua" type="text" class="form-control col-lg-8" name="namaketua" placeholder="Nama Ketua" value="{{ old('namaketua') }}" required>
                        @else
                        <input id="namaketua" type="text" class="form-control col-lg-8" name="namaketua" placeholder="Nama Ketua" value="{{$user->namaketua}}" required>
                        @endif
                      </div></div>
                      <br>
                      <div class="form-group row add">
                        <label class="control-label col-sm-2">Kelas Ketua :</label>
                        <div class="col-sm-9">
                        @if($user->kelasketua == NULL)
                        <input id="kelasketua" type="text" class="form-control col-lg-8" name="kelasketua" placeholder="Kelas Ketua" value="{{ old('kelasketua') }}" required>
                        @else
                        <input id="kelasketua" type="text" class="form-control col-lg-8" name="kelasketua" placeholder="Kelas Ketua" value="{{$user->kelasketua}}" required>
                        @endif
                      </div></div>
                      <br>
                      <div class="form-group row add">
                        <label class="control-label col-sm-2">Nama Anggota 1 :</label>
                        <div class="col-sm-9">
                        @if($user->namaanggota1 == NULL)
                        <input id="namaanggota1" type="text" class="form-control col-lg-8" name="namaanggota1" placeholder="Nama Anggota 1" value="{{ old('namaanggota1') }}" >
                        @else
                        <input id="namaanggota1" type="text" class="form-control col-lg-8" name="namaanggota1" placeholder="Nama Anggota 1" value="{{$user->namaanggota1}}" >
                        @endif
                      </div></div>
                      <br>
                      <div class="form-group row add">
                        <label class="control-label col-sm-2">Kelas Anggota 1 :</label>
                        <div class="col-sm-9">
                        @if($user->kelasanggota1 == NULL)
                        <input id="kelasanggota1" type="text" class="form-control col-lg-8" name="kelasanggota1" placeholder="Kelas Anggota 1" value="{{ old('kelasanggota1') }}" >
                        @else
                        <input id="kelasanggota1" type="text" class="form-control col-lg-8" name="kelasanggota1" placeholder="Kelas Anggota 1" value="{{$user->kelasanggota1}}" >
                        @endif
                      </div></div>
                      <br>
                    


                      <button type="submit" class="btn btn-success">Submit</button>


                </form>



                </div>
          </div>

          </div>

    <script src="{{url('assets/js/vendor/jquery-2.1.4.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"></script>
    <script src="{{url('assets/js/plugins.js')}}"></script>
    <script src="{{url('assets/js/main.js')}}"></script>
    <script src="{{url('assets/js/dashboard.js')}}"></script>



</body>
</html>
