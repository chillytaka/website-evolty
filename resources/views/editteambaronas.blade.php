<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>DASHBOARD BARONAS 2019</title>
    <meta name="description" content="DASHBOARD ELECTRA 8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="og:image" content="{{url('assets/img/logoevolty.png')}}">
    <meta itemprop="image" content="{{url('assets/img/logoevolty.png')}}">
    <link rel="shortcut icon" href="{{asset('assets/img/evolty.png')}}" />

    <!-- <link rel="shortcut icon" href="favicon.ico">  isi icon electra nanti -->

    <link rel="stylesheet" href="{{url('dashboard/css/normalize.css')}}">
    <link rel="stylesheet" href="{{url('dashboard/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{url('dashboard/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{url('dashboard/css/themify-icons.css')}}">
    <link rel="stylesheet" href="{{url('dashboard/css/flag-icon.min.css')}}">
    <link rel="stylesheet" href="{{url('dashboard/css/cs-skin-elastic.css')}}">
    <!-- <link rel="stylesheet" href="assets/css/bootstrap-select.less"> -->
    <link rel="stylesheet" href="{{url('dashboard/scss/style.css')}}">


    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>



</head>
<body>

  @if(session()->has('message'))
  <script>alert(' {{ session()->get('message') }}')</script>
  @endif


    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">

            <div class="navbar-header">
                <p class="navbar-brand">BARONAS 2019</p>


            </div>

            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="active">
                        <a href="/home">Dashboard </a>
                    </li>
                    <h3 class="menu-title">Profile</h3>
                    <li>
                        <a href="/editteambaronas">INPUT DATA TIM</a>

                    </li>


                    <!-- //<h3 class="menu-title">Tambah Menu apa</h3> -->

                </ul>
            </div>
        </nav>
    </aside>



    <div id="right-panel" class="right-panel">


        <header id="header" class="header">

            <div class="header-menu">


                <div class="col-sm-12">
                    <div class="user-area dropdown float-right">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <p>Halo Team {{Auth::user()->name}}</p>
                        </a>

                        <div class="user-menu dropdown-menu">
                                <a class="nav-link" href="#"><i class="fa fa- user"></i>My Profile</a>
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                        </div>
                    </div>
                </div>
            </div>

        </header><!-- /header -->
        <!-- Header-->

        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>INPUT DATA TEAM</h1>
                    </div>
                </div>
            </div>

        </div>

        <div class="col-md-12">

          <div class="card">
              <div class="card-header">
                  <strong class="card-title">INPUT</strong>
              </div>
              <div class="card-body">


                <form method="POST" action="/home/editteambaronas/input" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div>
                      <input type="hidden" name="updateID" value="{{Auth::user()->id}}">

                    <div class="input-group">
                      <p class="my-auto col-lg-4">Nama Pembimbing :</p>
                      @if(Auth::user()->b_namapembimbing == NULL)
                        <input id="b_namapembimbing" type="text" class="form-control col-lg-8" name="b_namapembimbing" placeholder="Nama Pembimbing" value="{{ old('b_namapembimbing') }}" required>
                      @else
                        <input id="b_namapembimbing" type="text" class="form-control col-lg-8" name="b_namapembimbing" placeholder="Nama Pembimbing" value="{{Auth::user()->b_namapembimbing}}" required>
                      @endif
                    </div>
                  <br>

                    <div class="input-group">
                      <p class="my-auto col-lg-4">Nama Anggota 1 :</p>
                      @if(Auth::user()->b_namapeserta1 == NULL)
                        <input id="b_namapeserta1" type="text" class="form-control col-lg-8" name="b_namapeserta1" placeholder="Nama Anggota 1" value="{{ old('b_namapeserta1') }}" required>
                      @else
                        <input id="b_namapeserta1" type="text" class="form-control col-lg-8" name="b_namapeserta1" placeholder="Nama Anggota 1" value="{{Auth::user()->b_namapeserta1}}" required>
                      @endif
                    </div>
                  <br>

                  <div class="input-group" style="text-align: left;">
                    <p class="my-auto col-lg-4">Upload Kartu Pelajar Anggota 1</p>
                    <input type="file" name="b_ktppeserta1" value="" required/>
                  </div>
                  <span class="text-danger">
                    {{ $errors->first('baronas_kartupelajar1') }}
                  </span>
                  <br>

                  <div class="input-group" style="text-align: left;">
                    <p class="my-auto col-lg-4">Upload Foto Anggota 1</p>
                    <input type="file" name="b_fotopeserta1" value="" required/>
                  </div>
                  <span class="text-danger">
                    {{ $errors->first('baronas_foto1') }}
                  </span>
                  <br>

                  <div class="input-group">
                    <p class="my-auto col-lg-4">Nama Anggota 2 :</p>
                    @if(Auth::user()->b_namapeserta2 == NULL)
                      <input id="b_namapeserta2" type="text" class="form-control col-lg-8" name="b_namapeserta2" placeholder="Nama Anggota 2" value="{{ old('b_namapeserta2') }}" >
                    @else
                      <input id="b_namapeserta2" type="text" class="form-control col-lg-8" name="b_namapeserta2" placeholder="Nama Anggota 2" value="{{Auth::user()->b_namapeserta2}}" >
                    @endif
                  </div>
                <br>

                <div class="input-group" style="text-align: left;">
                  <p class="my-auto col-lg-4">Upload Kartu Pelajar Anggota 2</p>
                  <input type="file" name="b_ktppeserta2" value="" />
                </div>
                <span class="text-danger">
                  {{ $errors->first('baronas_kartupelajar2') }}
                </span>
                <br>

                <div class="input-group" style="text-align: left;">
                  <p class="my-auto col-lg-4">Upload Foto Anggota 2</p>
                  <input type="file" name="b_fotopeserta2" value="" />
                </div>
                <span class="text-danger">
                  {{ $errors->first('baronas_foto2') }}
                </span>
                <br>

                  <div class="input-group">
                   <p class="my-auto col-lg-4">Nama Anggota 3 :</p>
                    @if(Auth::user()->b_namapeserta3 == NULL)
                      <input id="b_namapeserta3" type="text" class="form-control col-lg-8" name="b_namapeserta3" placeholder="Nama Anggota 3" value="{{ old('b_namapeserta3') }}" >
                    @else
                      <input id="b_namapeserta3" type="text" class="form-control col-lg-8" name="b_namapeserta3" placeholder="Nama Anggota 3" value="{{Auth::user()->b_namapeserta3}}" >
                    @endif
                  </div>
                <br>

                <div class="input-group" style="text-align: left;">
                  <p class="my-auto col-lg-4">Upload Kartu Pelajar Anggota 3</p>
                  <input type="file" name="b_ktppeserta3" value="" />
                </div>
                <span class="text-danger">
                  {{ $errors->first('baronas_kartupelajar3') }}
                </span>
                <br>

                <div class="input-group" style="text-align: left;">
                  <p class="my-auto col-lg-4">Upload Foto Anggota 3</p>
                  <input type="file" name="b_fotopeserta3" value="" />
                </div>
                <span class="text-danger">
                  {{ $errors->first('baronas_foto3') }}
                </span>
                <br>

                  <div class="input-group">
                    <p class="my-auto col-lg-4">Asal Sekolah :</p>
                    @if(Auth::user()->asalsekolah == NULL)
                      <input id="asalsekolah" type="text" class="form-control col-lg-8" name="asalsekolah" placeholder="Asal Sekolah" value="{{ old('asalsekolah') }}" required>
                    @else
                      <input id="asalsekolah" type="text" class="form-control col-lg-8" name="asalsekolah" placeholder="Asal Sekolah" value="{{Auth::user()->asalsekolah}}" required>
                    @endif
                  </div>
                <br>

                  <div class="input-group">
                    <p class="my-auto col-lg-4">Alamat Sekolah :</p>
                    @if(Auth::user()->alamatsekolah == NULL)
                      <input id="alamatsekolah" type="text" class="form-control col-lg-8" name="alamatsekolah" placeholder="Alamat Sekolah" value="{{ old('alamatsekolah') }}" required>
                    @else
                      <input id="alamatsekolah" type="text" class="form-control col-lg-8" name="alamatsekolah" placeholder="Alamat Sekolah" value="{{Auth::user()->alamatsekolah}}" required>
                    @endif
                  </div>
                <br>

                  <div class="input-group">
                    <p class="my-auto col-lg-4">Email :</p>
                    @if(Auth::user()->email == NULL)
                      <input id="email" type="text" class="form-control col-lg-8" name="email" placeholder="Email" value="{{ old('email') }}" required>
                    @else
                      <input id="email" type="text" class="form-control col-lg-8" name="email" placeholder="Email" value="{{Auth::user()->email}}" required>
                    @endif
                  </div>
                <br>
                <div class="input-group">
                  <p class="my-auto col-lg-4">Note : </p>
                  <p class="col-lg-8"> Semua Gambar Max Size 2 MB </p>

                </div>
              <br>

          <div class="modal-footer">
            <button type="submit" class="btn btn-success">Masukkan Data Peserta</button>

          </div>
      </form>


                </div>
          </div>

          </div>

    <script src="{{url('assets/js/vendor/jquery-2.1.4.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"></script>
    <script src="{{url('assets/js/plugins.js')}}"></script>
    <script src="{{url('assets/js/main.js')}}"></script>
    <script src="{{url('assets/js/dashboard.js')}}"></script>



</body>
</html>
