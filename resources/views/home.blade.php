<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    @if(Auth::user()->event == "electra")
    <title>DASHBOARD ELECTRA</title>
    <meta name="description" content="DASHBOARD ELECTRA 8">
    @else
    <title>DASHBOARD BARONAS</title>
    <meta name="description" content="DASHBOARD BARONAS 2019">
    @endif
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="og:image" content="{{url('assets/img/logoevolty.png')}}">
    <meta itemprop="image" content="{{url('assets/img/logoevolty.png')}}">
    <link rel="shortcut icon" href="{{asset('assets/img/evolty.png')}}" />

    <!-- <link rel="shortcut icon" href="favicon.ico">  isi icon electra nanti -->

    <link rel="stylesheet" href="{{url('dashboard/css/normalize.css')}}">
    <link rel="stylesheet" href="{{url('dashboard/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{url('dashboard/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{url('dashboard/css/themify-icons.css')}}">
    <link rel="stylesheet" href="{{url('dashboard/css/flag-icon.min.css')}}">
    <link rel="stylesheet" href="{{url('dashboard/css/cs-skin-elastic.css')}}">
    <!-- <link rel="stylesheet" href="assets/css/bootstrap-select.less"> -->
    <link rel="stylesheet" href="{{url('dashboard/scss/style.css')}}">


    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>



</head>
<body>


  @if(session()->has('message'))
  <script>alert(' {{ session()->get('message') }}')</script>
  @endif

    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">

            <div class="navbar-header">
                @if(Auth::user()->event == "electra")
                <p class="navbar-brand">ELECTRA 8</p>
                @else
                <p class="navbar-brand">BARONAS 2019</p>
                @endif

            </div>

            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="active">
                        <a href="/home">Dashboard </a>
                    </li>
                      @if(Auth::User()->event == "electra")
                    <h3 class="menu-title">Profile</h3>
                    <li>
                        <a href="/home/editteam">EDIT DATA TIM</a>

                    </li>
                    @else
                    <h3 class="menu-title">Profile</h3>
                    <li>
                        <a href="/home/editteambaronas">INPUT DATA TIM</a>

                    </li>
                    @endif
                    <!-- <h3 class="menu-title">Tambah Menu apa</h3> -->

                </ul>
            </div>
        </nav>
    </aside>



    <div id="right-panel" class="right-panel">


        <header id="header" class="header">

            <div class="header-menu">


                <div class="col-sm-12">
                    <div class="user-area dropdown float-right">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <p>LOGOUT</p>
                        </a>

                        <div class="user-menu dropdown-menu">

                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                        </div>
                    </div>
                </div>
            </div>

        </header><!-- /header -->
        <!-- Header-->

        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Dashboard</h1>
                    </div>
                </div>
            </div>

        </div>






        <div class="col-md-12">
            @if(Auth::User()->event == "electra")
            <div class="card">
                <div class="card-header">
                    <strong class="card-title">Status</strong>
                </div>
                <div class="card-body">

                    @if(Auth::user()->status == 1)
                    <p>Selamat TIM {{Auth::user()->name}} , TIM kamu sudah berhasil di verifikasi :)</p>
                    @else
                      <p>Halo TIM {{Auth::user()->name}} , TIM Kamu Belum terverifikasi, Di tunggu aja ya :)</p>
                    @endif
                  </div>
            </div>

                       <div class="card">
                           <div class="card-header">
                               <strong class="card-title">Pengumuman</strong>
                           </div>
                           <div class="card-body">

                               <p class="card-text">
                                 SELAMAT DATANG TIM
                                 {{Auth::user()->name}}<br>
                                 1. KAMU MEMILIH REGION : {{Auth::user()->region}}
                                 <br>
                                 TEMPAT TES :
                                 @if (Auth::user()->region == 'Surabaya' )
                                 ELEKTRO ITS
                                 @elseif (Auth::user()->region == 'Online' )
                                 WEB EVOLTY ITS 2019
                                 @else
                                 TEMPAT MENYUSUL
                                 @endif


                               </p>


                           </div>
                       </div>
                       <?php $user=Auth::user(); ?>

                       <div class="card">
                           <div class="card-header">
                               <strong class="card-title">DATA TEAM</strong>
                           </div>
                           <div class="card-body">


                                <br>
                                   <div class="form-group row add">
                                     <label class="control-label col-sm-2">Nama Tim :</label>
                                     <div class="col-sm-9">
                                     @if($user->name == NULL)
                                     <input id="name" type="text" class="form-control col-lg-8" name="name" placeholder="Nama Tim" value="{{ old('name') }}" disabled>
                                     @else
                                     <input id="name" type="text" class="form-control col-lg-8" name="name" placeholder="Nama Tim" value="{{$user->name}}" disabled>
                                     @endif
                                   </div></div>
                                   <br>
                                      <div class="form-group row add">
                                        <label class="control-label col-sm-2">No Peserta:</label>
                                        <div class="col-sm-9">
                                        @if($user->no_peserta == NULL)
                                        <input id="no_peserta" type="text" class="form-control col-lg-8" name="no_peserta" placeholder="no_peserta" value="{{ old('no_peserta') }}" disabled>
                                        @else
                                        <input id="no_peserta" type="text" class="form-control col-lg-8" name="no_peserta" placeholder="no_peserta" value="{{$user->no_peserta}}" disabled>
                                        @endif
                                      </div></div>
                                   <br>
                                   <div class="form-group row add">
                                     <label class="control-label col-sm-2">Email :</label>
                                     <div class="col-sm-9">
                                     @if($user->email == NULL)
                                     <input id="email" type="text" class="form-control col-lg-8" name="email" placeholder="Email" value="{{ old('email') }}" disabled>
                                     @else
                                     <input id="email" type="text" class="form-control col-lg-8" name="email" placeholder="Email" value="{{$user->email}}" disabled>
                                     @endif
                                   </div></div>
                                   <br>


                                   <div class="form-group row add">
                                     <label class="control-label col-sm-2">Nomor Telepon :</label>
                                     <div class="col-sm-9">
                                     @if($user->notelp == NULL)
                                     <input id="notelp" type="text" class="form-control col-lg-8" name="notelp" placeholder="Nomor Telepon" value="{{ old('notelp') }}" disabled>
                                     @else
                                     <input id="notelp" type="text" class="form-control col-lg-8" name="notelp" placeholder="Nomor Telepon" value="{{$user->notelp}}" disabled>
                                     @endif
                                   </div></div>
                                    <br>
                                   <div class="form-group row add">
                                     <label class="control-label col-sm-2">Asal Sekolah :</label>
                                     <div class="col-sm-9">
                                     @if($user->asalsekolah == NULL)
                                     <input id="asalsekolah" type="text" class="form-control col-lg-8" name="asalsekolah" placeholder="Asal Sekolah" value="{{ old('asalsekolah') }}" disabled>
                                     @else
                                     <input id="asalsekolah" type="text" class="form-control col-lg-8" name="asalsekolah" placeholder="Asal Sekolah" value="{{$user->asalsekolah}}" disabled>
                                     @endif
                                   </div></div>
                                   <br>
                                   <div class="form-group row add">
                                     <label class="control-label col-sm-2">Alamat Sekolah :</label>
                                     <div class="col-sm-9">
                                     @if($user->alamatsekolah == NULL)
                                     <input id="alamatsekolah" type="text" class="form-control col-lg-8" name="alamatsekolah" placeholder="Alamat Sekolah" value="{{ old('alamatsekolah') }}" disabled>
                                     @else
                                     <input id="alamatsekolah" type="text" class="form-control col-lg-8" name="alamatsekolah" placeholder="Alamat Sekolah" value="{{$user->alamatsekolah}}" disabled>
                                     @endif
                                   </div></div>
                                   <br>
                                   <div class="form-group row add">
                                     <label class="control-label col-sm-2">Nama Ketua :</label>
                                     <div class="col-sm-9">
                                     @if($user->namaketua == NULL)
                                     <input id="namaketua" type="text" class="form-control col-lg-8" name="namaketua" placeholder="Nama Ketua" value="{{ old('namaketua') }}" disabled>
                                     @else
                                     <input id="namaketua" type="text" class="form-control col-lg-8" name="namaketua" placeholder="Nama Ketua" value="{{$user->namaketua}}" disabled>
                                     @endif
                                   </div></div>
                                   <br>
                                   <div class="form-group row add">
                                     <label class="control-label col-sm-2">Kelas Ketua :</label>
                                     <div class="col-sm-9">
                                     @if($user->kelasketua == NULL)
                                     <input id="kelasketua" type="text" class="form-control col-lg-8" name="kelasketua" placeholder="Kelas Ketua" value="{{ old('kelasketua') }}" disabled>
                                     @else
                                     <input id="kelasketua" type="text" class="form-control col-lg-8" name="kelasketua" placeholder="Kelas Ketua" value="{{$user->kelasketua}}" disabled>
                                     @endif
                                   </div></div>
                                   <br>
                                   <div class="form-group row add">
                                     <label class="control-label col-sm-2">Nama Anggota 1 :</label>
                                     <div class="col-sm-9">
                                     @if($user->namaanggota1 == NULL)
                                     <input id="namaanggota1" type="text" class="form-control col-lg-8" name="namaanggota1" placeholder="Nama Anggota 1" value="{{ old('namaanggota1') }}" disabled>
                                     @else
                                     <input id="namaanggota1" type="text" class="form-control col-lg-8" name="namaanggota1" placeholder="Nama Anggota 1" value="{{$user->namaanggota1}}" disabled>
                                     @endif
                                   </div></div>
                                   <br>
                                   <div class="form-group row add">
                                     <label class="control-label col-sm-2">Kelas Anggota 1 :</label>
                                     <div class="col-sm-9">
                                     @if($user->kelasanggota1 == NULL)
                                     <input id="kelasanggota1" type="text" class="form-control col-lg-8" name="kelasanggota1" placeholder="Kelas Anggota 1" value="{{ old('kelasanggota1') }}" disabled>
                                     @else
                                     <input id="kelasanggota1" type="text" class="form-control col-lg-8" name="kelasanggota1" placeholder="Kelas Anggota 1" value="{{$user->kelasanggota1}}" disabled>
                                     @endif
                                   </div></div>
                                   <br>
                             </div>
                       </div>

                       <div class="card">
                           <div class="card-header">
                               <strong class="card-title">Time Remaining :</strong>
                           </div>
                           <div class="card-body">

                           <p id="countdown" style="color: black;text-align: center;font-size: 60px;margin-top: 0px;"></p>

                             </div>
                       </div>

                      @else
                      <div class="card">
                          <div class="card-header">
                              <strong class="card-title">Pengumuman</strong>
                          </div>
                          <div class="card-body">

                              <p class="card-text">
                                SELAMAT DATANG TIM
                                {{Auth::user()->name}}<br>
                                Terimakasih telah mendaftar BARONAS 2019<br>
                                Untuk Berikutnya Mohon untuk melengkapi data diri tim di menu input data team
                              </p>


                          </div>
                      </div>
                      <div class="card">
                          <div class="card-header">
                              <strong class="card-title">Status Data Team</strong>
                          </div>
                          <div class="card-body">

                              @if(Auth::user()->b_ktppeserta1 != NULL && Auth::user()->b_ktppeserta2 != NULL && Auth::user()->b_ktppeserta3 != NULL && Auth::user()->b_fotopeserta1 != NULL && Auth::user()->b_fotopeserta2 != NULL && Auth::user()->b_fotopeserta3 != NULL)
                              <p>Selamat TIM {{Auth::user()->name}} , TIM kamu sudah berhasil Input Data Team :)</p>
                              @else
                                <p>Halo TIM {{Auth::user()->name}} , TIM Kamu Belum Input Data Team, Silakan input di menu input data team</p>
                              @endif
                            </div>
                      </div>

                      @if(Auth::user()->b_ktppeserta1 != NULL && Auth::user()->b_ktppeserta2 != NULL && Auth::user()->b_ktppeserta3 != NULL && Auth::user()->b_fotopeserta1 != NULL && Auth::user()->b_fotopeserta2 != NULL && Auth::user()->b_fotopeserta3 != NULL)
                      <div class="card">
                          <div class="card-header">
                              <strong class="card-title">Rule</strong>
                          </div>
                          <div class="card-body">
                              <p class="card-text">
                                Rule Untuk kategory {{Auth::user()->kategory}} :
                                @if (Auth::user()->kategory == "SD")
                                <button class="btn btn-success"><a href="https://intip.in/RulesElementary" target="_blank" style="color:black;">View</a></button>
                                @elseif (Auth::user()->kategory == "SMP")
                                <button class="btn btn-success"><a href="https://intip.in/RulesJunior" target="_blank" style="color:black;">View</a></button>
                                @elseif (Auth::user()->kategory == "SMA")
                                <button class="btn btn-success"><a href="https://intip.in/RulesSenior" target="_blank" style="color:black;">View</a></button>
                                @elseif (Auth::user()->kategory == "OPEN")
                                <button class="btn btn-success"><a href="https://intip.in/RulesOpen" target="_blank" style="color:black;">View</a></button>
                                @endif
                              </p>
                          </div>
                      </div>
                      @else
                      <div class="card">
                          <div class="card-header">
                              <strong class="card-title">Rule</strong>
                          </div>
                          <div class="card-body">
                              <p class="card-text">
                                Input Data Team dahulu, baru rule akan muncul.
                              </p>
                          </div>
                      </div>
                      @endif

                @endif


    </div><!-- /#right-panel -->

    <!-- Right Panel -->

    <script src="{{url('assets/js/vendor/jquery-2.1.4.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"></script>
    <script src="{{url('assets/js/plugins.js')}}"></script>
    <script src="{{url('assets/js/main.js')}}"></script>
    <script>
    var countDownDate = new Date("Feb 5, 2019 08:00:00").getTime();

    // Update the count down every 1 second
    var x = setInterval(function() {

        // Get todays date and time
        var now = new Date().getTime();

        // Find the distance between now and the count down date
        var distance = countDownDate - now;

        // Time calculations for days, hours, minutes and seconds
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);


        document.getElementById("countdown").innerHTML = days + "d " + hours + "h "
        + minutes + "m " + seconds + "s ";


        if (distance < 0) {
            clearInterval(x);
            document.getElementById("countdown").innerHTML = "EXPIRED";
        }
    }, 1000);
    </script>



    <script src="{{url('assets/js/dashboard.js')}}"></script>



</body>
</html>
