<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="EVOLTY 2019 adalah rangkaian event terbesar di ITS yang diselenggarakan oleh Fakultas Teknologi Elektro Institut Teknologi Sepuluh Nopember Surabaya untuk mewadahi dan meningkatkan atmosfir kompetitif pelajar se-Indonesia khususnya dalam rumpun ilmu elektro.">
    <meta name="author" content="FTE-ITS">
    <meta property="og:image" content="{{url('assets/img/logoevolty.png')}}">
    <meta itemprop="image" content="{{url('assets/img/logoevolty.png')}}">

    <title>@yield('title')</title>
    <link rel="stylesheet" href="{{url('css/style.css')}}"> <!-- Resource style -->

    <!-- Bootstrap core CSS -->
    <link href="{{url('vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">


    <!-- Custom styles for this template -->
    <link href="{{url('css/scrolling-nav.css')}}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{url('css/fullpage.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('css/mouse-anim.css')}}">
    <link href='http://fonts.googleapis.com/css?family=Ubuntu:300,400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">


    <script src="{{url('js/modernizr.js')}}"></script> <!-- Modernizr -->

  </head>

  <body id="page-top">

    <div id="fullpage">
        <div class="section" >
          @yield('section1')
        </div>
        <div class="section">
          @yield('section2')
        </div>
        <div class="section" style="text-align:center">
          @yield('section3')
        </div>
        <div class="section" style="text-align:center"  >
          @yield('section5')
        </div>
        <div class="section"  >
          @yield('section6')
        </div>
    </div>


    <!-- Bootstrap core JavaScript -->
    <script src="{{url('vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{url('vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

    <!-- Plugin JavaScript -->
    <script src="{{url('vendor/jquery-easing/jquery.easing.min.js')}}"></script>

    <!-- Custom JavaScript for this theme -->
    <script src="{{url('js/scrolling-nav.js')}}"></script>
    <script src="{{url('js/fullpage.js')}}"></script>
    <script src="{{url('js/parallax.js')}}"></script>
    <script src="{{url('js/jquery-2.1.1.js')}}"></script>
    <script src="{{url('js/main.js')}}"></script> <!-- Resource jQuery -->

    <!-- <script src="{{url('js/particles.js')}}"></script> -->
    <!-- <script src="{{url('js/app.js')}}"></script> -->

  </body>

</html>
