<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="https://www.evoltyits.com/assets/img/evolty.png">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- <title>{{ config('app.name', 'Register') }}</title> -->
    <title>Login</title>

    <!-- Scripts -->



    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link rel="stylesheet" href="https://www.evoltyits.com/css/style.css"> <!-- Resource style -->
    <!-- Bootstrap core CSS -->
    <link href="{{url('vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Custom styles for this template -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <style>


    .container{
    height: 100%;
    align-content: center;
    }

    .card{
    margin-top: auto;
    margin-bottom: auto;
    background-color: rgba(0,0,0,0.5) !important;
    color: white;
    }

    </style>

</head>

<body class="background" >
    <div id="app">
                            <nav class="navbar navbar-expand-lg fixed-top " style="background-color: #1B1B1B;"
                        id="mainNav" >
                            <div class="container">
                                <a class="navbar-brand js-scroll-trigger" href="https://www.evoltyits.com/"><img src="https://www.evoltyits.com/assets/img/evolty.png" style="height: 50px">
                        </a>

                                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive">
                                <span class="navbar-toggler-icon"><i class="material-icons" style="color:white">menu</i></span>
                                </button>

                                <div class="collapse navbar-collapse" id="navbarResponsive">
                                <ul class="nav navbar-nav ml-auto">
                                  <li class="nav-item">
                                    <a class="nav-link js-scroll-trigger link-text " href="https://www.evoltyits.com/#page-top">HOME</a>
                                  </li>
                                  <li class="nav-item">
                                    <a class="nav-link js-scroll-trigger" href="https://www.evoltyits.com/#section2">THEME</a>

                                  </li>
                                  <li class="nav-item">
                                    <a class="nav-link js-scroll-trigger" href="https://www.evoltyits.com/#section3">ELECTRA</a>
                                  </li>
                                  <li class="nav-item">
                                    <a class="nav-link js-scroll-trigger" href="https://www.evoltyits.com/#section4">BARONAS</a>
                                  </li>
                                  <li class="nav-item">
                                    <a class="nav-link js-scroll-trigger" href="https://www.evoltyits.com/#section5">MAGE4</a>
                                  </li>

                        @guest
                            <li class="nav-item">
                                <a class="nav-link active" href="{{ route('login') }}">{{ __('LOGIN') }}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">{{ __('REGISTER') }}</a>
                            </li>
                        @else
                        <li class="nav-item">
                          <a class="nav-link js-scroll-trigger" href="/home">DASHBOARD</a>
                        </li>

                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                   {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-5">
            @yield('content')
        </main>
    </div>
    <script src="{{url('vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{url('vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{url('js/otomatisregion2.js')}}"></script>
    <script src="{{url('js/particles.js')}}"></script>
    <script src="{{url('js/app.js')}}"></script>
</body>
</html>
