
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />

		<title>DASHBOARD ADMIN</title>

		<meta name="description" content="DASHBOARD ADMIN" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
		<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="shortcut icon" href="{{asset('assets/img/evolty.png')}}" />
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <!-- CSRF Token -->

		<!-- bootstrap & fontawesome -->
		<link rel="stylesheet" href="{{url('assets/css/bootstrap.min.css')}}" />
		<link rel="stylesheet" href="{{url('assets/font-awesome/4.2.0/css/font-awesome.min.css')}}" />
		<link rel="stylesheet" href="{{url('assets/css/datepicker.min.css')}}" />
		<link rel="stylesheet" href="{{url('assets/datatables/css/dataTables.bootstrap.css')}}" >
		<!-- page specific plugin styles -->
		<link href="{{url('assets/autocomplete/easy-autocomplete.css')}}" rel="stylesheet" type="text/css">

		<!-- text fonts -->
		<link rel="stylesheet" href="{{url('assets/fonts/fonts.googleapis.com.css')}}" />

		<!-- ace styles -->
		<link rel="stylesheet" href="{{url('assets/css/ace.min.css')}}" class="ace-main-stylesheet" id="main-ace-style" />


		<!--[if lte IE 9]>
			<link rel="stylesheet" href="assets/css/ace-part2.min.css" class="ace-main-stylesheet" />
		<![endif]-->

		<!--[if lte IE 9]>
		  <link rel="stylesheet" href="assets/css/ace-ie.min.css" />
		<![endif]-->

		<!-- inline styles related to this page -->

		<!-- ace settings handler -->
		<script src="{{url('assets/js/ace-extra.min.js')}}"></script>
		<script src="{{url('https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js')}}"></script>
		<script src="{{url('assets/chartjs/dist/Chart.bundle.js')}}"></script>
		<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

		<!--[if lte IE 8]>
		<script src="assets/js/html5shiv.min.js"></script>
		<script src="assets/js/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="no-skin">
		<div id="navbar" class="navbar navbar-default navbar-fixed-top navbar-collapse">
			<script type="text/javascript">
				try{ace.settings.check('navbar' , 'fixed')}catch(e){}
			</script>

			<div class="navbar-container" id="navbar-container">
				<button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
					<span class="sr-only">Toggle Sidebar</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<div class="pull-right">
					<ul class="nav ace-nav">

						<li class="light-blue">
							<a data-toggle="dropdown"  href="#" class="dropdown-toggle" >
								<img class="nav-user-photo hidden-xs" src="assets/img/avatar.png"/>
								<span class="user-info">
									<small>Selamat Datang</small>
														{{ Auth::user()->name }}			</span>
								<i class="ace-icon fa fa-caret-down"></i>
							</a>
														<ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">

								<li>
                  <a class="dropdown-item" href="{{ route('logout') }}"
                     onclick="event.preventDefault();
                                   document.getElementById('logout-form').submit();">
                      {{ __('Logout') }}
                  </a>

                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      @csrf
                  </form>
								</li>
							</ul>
													</li>
					</ul>
				</div>
				<div class="navbar-header pull-left">
					<a href="/adminarea" class="navbar-brand">
						<small>
						<img src="" />
							<span><b>DASHBOARD ADMIN EVOLTY</b></span>
						</small>
					</a>
				</div>
			</div><!-- /.navbar-container -->
		</div>
		<div class="main-container" id="">
			<script type="text/javascript">
				try{ace.settings.check('main-container' , 'relatif')}catch(e){}
			</script>
						<div id="sidebar" class="sidebar responsive sidebar-fixed">
				<script type="text/javascript">
					try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
				</script>

				<div class="sidebar-shortcuts" id="sidebar-shortcuts">
					<div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large" style="padding-top:3px;">
						<b>DAFTAR MENU</b>
					</div>
				</div><!-- /.sidebar-shortcuts -->
				<ul class="nav nav-list">
					<li >
						<a href="#" class="dropdown-toggle">
							<i class="menu-icon fa fa-child blue"></i>
							<span class="menu-text blue">Electra</span>
							<b class="arrow fa fa-angle-down"></b>
						</a>
						<b class="arrow"></b>
						<ul class="submenu">
						<li >
							<a href="/adminelectra?page=1">
								Daftar Peserta
							</a>
							<b class="arrow"></b>
						</li>
						<li >
							<a href="/tambahpeserta_e">
								Tambah Peserta
							</a>
							<b class="arrow"></b>
						</li>
						<li >
							<a href="/pesertaneedverif?page=1">
								Peserta Need verif
							</a>
							<b class="arrow"></b>
						</li>
						<li >
							<a href="/exportelectra">
								Peserta Export CSV
							</a>
							<b class="arrow"></b>
						</li>
						</ul>
					</li>

					<li >
					  <a href="#" class="dropdown-toggle">
					    <i class="menu-icon fa green fa-child"></i>
					    <span class="menu-text green">Baronas</span>
					    <b class="arrow fa fa-angle-down"></b>
					  </a>
					  <b class="arrow"></b>
            <ul class="submenu">
						<li >
							<a href="/adminbaronas?page=1">
								Daftar Peserta udah dikasih pass
							</a>
							<b class="arrow"></b>
						</li>
						<li >
							<a href="">
								Tambah Peserta
							</a>
							<b class="arrow"></b>
						</li>
						<li >
							<a href="/pesertaneedpass?page=1">
								Peserta Need Pass
							</a>
							<b class="arrow"></b>
						</li>
          </ul>

					</li>
				</ul><!-- /.nav-list -->

				<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
					<i class="ace-icon fa fa-angle-double-left" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
				</div>

				<script type="text/javascript">
					try{ace.settings.check('sidebar' , 'collapsed')}catch(e){}
				</script>
			</div>



      <div class="main-content">
				<div class="main-content-inner">
					<div class="page-content">
						@yield('main-content')
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->





 			<div class="footer">
				<div class="footer-inner">
					<div class="footer-content">
						<span class="bigger-60">
							<span class="blue bolder">TIM WEB EVENT</span>
							&copy; 2019
						</span>
					</div>
				</div>
			</div>

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

	<!-- basic scripts -->

	<!--[if !IE]> -->
	<script src="{{url('assets/js/jquery.2.1.1.min.js')}}"></script>


	<!--[if IE]>
	<script src="assets/js/jquery.1.11.1.min.js"></script>
	<![endif]-->

	<!--[if !IE]> -->
	<script type="text/javascript">
		window.jQuery || document.write("<script src='assets/js/jquery.min.js'>"+"<"+"/script>");
	</script>

	<!-- <![endif]-->

	<!--[if IE]>
	<script type="text/javascript">
	 window.jQuery || document.write("<script src='assets/js/jquery1x.min.js'>"+"<"+"/script>");
	</script>
	<![endif]-->
	<script type="text/javascript">
		if('ontouchstart' in document.documentElement) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
	</script>
	<script src="{{url('assets/js/bootstrap.min.js')}}"></script>

	<!-- page specific plugin scripts -->
	<script src="{{url('assets/js/jquery-ui.min.js')}}"></script>

	<!-- ace scripts -->
	<script src="{{url('assets/js/ace-elements.min.js')}}"></script>
	<script src="{{url('assets/js/ace.min.js')}}"></script>
	<script src="{{url('assets/js/fuelux.spinner.min.js')}}"></script>


	<script src="{{url('assets/autocomplete/jquery.easy-autocomplete.js')}}" type="text/javascript" ></script>

</body>
</html>
