<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="EVOLTY 2019 adalah rangkaian event terbesar di ITS yang diselenggarakan oleh Fakultas Teknologi Elektro Institut Teknologi Sepuluh Nopember Surabaya untuk mewadahi dan meningkatkan atmosfir kompetitif pelajar se-Indonesia khususnya dalam rumpun ilmu elektro.">
    <meta name="author" content="FTE-ITS">
    <meta property="og:image" content="{{url('assets/img/logoevolty.png')}}">
    <meta itemprop="image" content="{{url('assets/img/logoevolty.png')}}">
    <link rel="shortcut icon" href="{{asset('assets/img/evolty.png')}}" />

    <title>@yield('title')</title>
    <link rel="stylesheet" href="{{url('css/style.css')}}"> <!-- Resource style -->
    <link rel="stylesheet" type="text/css" href="{{url('css/fullpage.css')}}" />



    <!-- Bootstrap core CSS -->
    <link href="{{url('vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">


    <!-- Custom styles for this template -->
    <link href="{{url('css/scrolling-nav.css')}}" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Ubuntu:300,400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">


  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
    <script src="{{url('js/modernizr.js')}}"></script> <!-- Modernizr -->

  </head>

  <body id="page-top">

       <!-- Navigation -->
    <nav class="navbar navbar-expand-lg fixed-top " @yield('navbar-style') id="mainNav" >
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="#page-top">@yield('image-header')</a>

        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive">
          <span class="navbar-toggler-icon"><i class="material-icons" style="color:white">menu</i></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="nav navbar-nav ml-auto">
            @yield('nav-item')
          </ul>
        </div>
      </div>
    </nav>

<div id="fullpage">
    <div id="section1" class="cd-section background section" >
      @yield('section1')

    </div><!-- cd-section -->

    <style>
    @media (min-width: 768px) {
    #section2 {
  background-image:url('/assets/img/coba_theme.png');background-color:white;background-repeat:no-repeat;
    }
    }
    @media (max-width: 765px) {
    #section2 {
       background-image: none;
       background-color:white;
    }
    }
    </style>

    <div id="section2" class="cd-section text-center section">
     @yield('section2')
    </div><!-- cd-section -->

    <div id="section3" class="section" style="background-color:#1b1b1b" >
       @yield('section3')
    </div>

    <style>
    @media (min-width: 1280px) {
  #section4 {
    background-image: url('/assets/img/baronas_coba.png');
  }
}
@media (max-width: 1279px) {
  #section4 {
       background-image: none;
  }
}
    </style>
    <div id="section4" class="section">
       @yield('section4')
    </div>
    <div id="section5" class="section" style="background-color:#1b1b1b" >
       @yield('section5')
    </div>





    <footer id="section6" style="background-color: #1b1b1b;min-height:30vh" class="section text-white">
        <div class="container">
          <div class="row">
            @yield('footer')
          </div>
        </div>
      </footer>
</div>
    <!-- Bootstrap core JavaScript -->
    <script src="{{url('vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{url('vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

    <!-- Plugin JavaScript -->
    <script src="{{url('vendor/jquery-easing/jquery.easing.min.js')}}"></script>

    <!-- Custom JavaScript for this theme -->
    <script src="{{url('js/scrolling-nav.js')}}"></script>
    <script src="{{url('js/parallax.js')}}"></script>
    <script src="{{url('js/jquery-2.1.1.js')}}"></script>
    <script src="{{url('js/main.js')}}"></script> <!-- Resource jQuery -->

    <script src="{{url('js/particles.js')}}"></script>
    <script src="{{url('js/app.js')}}"></script>
    <script type="text/javascript" src="{{url('js/fullpage.js')}}"></script>
    <script type="text/javascript" src="{{url('vendor/scrolloverflow.min.js')}}"></script>


<script src="{{url('vendor/easings.min.js')}}"></script>

  </body>

</html>
