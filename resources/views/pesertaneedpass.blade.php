@extends('layouts.admin')

@section('main-content')
<div class="row">
  <div class="">
    <div class="">
      <h5 class=""><i class="ace-icon fa fa-floppy-o green"></i> <b>ADMIN BARONAS</b></h5>
    </div>
    <div class="">
      <div class="">
      <div class="row">
      <div class="col-xs-12">
        <!-- <a style="margin-bottom:4px;" class="btn btn-primary btn-white btn-round" href="/tambahpeserta_e"><i class="ace-icon fa fa-plus-circle bigger-100"></i>Tambah Peserta</a>
        <form action="/search" method="get">
    <input type="text" name="cari" class="form-group" id="search" placeholder="Search">

  <button type="submit"  class="btn btn-white btn-info btn-bold">
    <i class="ace-icon fa fa-search bigger-110"></i>
    Cari Data
  </button>
</form>
<p style="color:blue;">DATA yang bisa dicari : Nama Team, Region, Email, Asal Sekolah, Tipetes, Alamat Sekolah. Bila nambah data yg dicari bilang aja</p>
        <div style="margin-top:4px;"> -->

        <table id="aa" name="table" class="table table-fixed table-striped table-bordered " cellspacing="0" width="100%">
        <thead>
          <tr>
            <th class="center">No</th>
            <th class="center">Bukti</th>
            <th class="center">Nama Tim</th>

            <th class="center">Email</th>
            <th class="center">CP</th>
            <th class="center">Kategory</th>

              <th class="center">Kasih Pass</th>
              <th class="center">Hapus</th>
          </tr>
        </thead>
        <tbody>
          <?php $no=1; ?>

          @foreach($hasil2 as $user)
           <tr class="tablecontent">


             <td> {{ (($_GET['page']-1)*25)+$no }}</td>
              <?php $no++; ?>

             <td>    <a href="nope/gambar/buktibaronas/{{$user->bukti}}" target="_blank">{{$user->bukti}}</a></td>

             <td>{{$user->name}}</td>

             <td>{{$user->email}}</td>
             <td>{{$user->notelp}}</td>
             <td>{{$user->kategory}}</td>

             <td>
               <button type="button" data-toggle="modal" data-target="#{{ $user->id }}" data-uid="{{$user->id}} " class="update btn btn-warning btn-sm">Kasih Pass</button>
                 <div id="{{ $user->id }}" class="modal fade" role="dialog">
                   <div class="modal-dialog">
                     <div class="modal-content">
                       <div class="modal-header">
                         <button type="button" class="close" data-dismiss="modal">×</button>
                         <h4 class="modal-title">Kasih Pass Peserta</h4>
                       </div>
                       <form method="post" action="/adminbaronas/kasihpass">
                         {{csrf_field()}}
                           <div class="modal-body" style="overflow-y: auto; max-height: 400px">
                             <div class="form-group row add">
                               <label class="control-label col-sm-2"> Nomor Peserta :</label>
                               <div class="col-sm-9">
                               <input type="hidden" name="updateID" value="{{$user->id}}"/>
                               @if($user->no_peserta == NULL)
                               <input id="no_peserta" type="text" class="no_peserta  form-control" name="no_peserta" value="{{ old('no_peserta') }}" required>
                               @else
                               <input id="no_peserta" type="text" class="no_peserta  form-control" name="no_peserta" value="{{$user->no_peserta}}" required>
                               @endif
                             </div>
                           </div>

                             <br>
                             <div class="form-group row add">
                               <label class="control-label col-sm-2">Nama Tim :</label>
                               <div class="col-sm-9">
                               @if($user->name == NULL)
                               <input id="name" type="text" class="form-control col-lg-8" name="name" placeholder="Nama Tim" value="{{ old('name') }}" required>
                               @else
                               <input id="name" type="text" class="form-control col-lg-8" name="name" placeholder="Nama Tim" value="{{$user->name}}" required>
                               @endif
                             </div></div>
                             <br>
                             <div class="form-group row add">
                               <label class="control-label col-sm-2">Email :</label>
                               <div class="col-sm-9">
                               @if($user->email == NULL)
                               <input id="email" type="text" class="form-control col-lg-8" name="email" placeholder="Email" value="{{ old('email') }}" required>
                               @else
                               <input id="email" type="text" class="form-control col-lg-8" name="email" placeholder="Email" value="{{$user->email}}" required>
                               @endif
                             </div></div>

                             <br>
                             <div class="form-group row add">
                               <label class="control-label col-sm-2">Password :</label>
                               <div class="col-sm-9">

                               <input id="password" type="text" class="form-control col-lg-8" name="password" placeholder="pass" required>

                             </div></div>

                             <input id="status" type="hidden" class="form-control col-lg-8" name="status" value="1">


                           <div class="modal-footer">
                             <button type="submit" class="btn btn-success">SUBMIT</button>
                             <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                           </div>
                       </form>
                     </div>
                   </div>
                 </div>
             </td>

             <td>
               <form method="post" action="/adminbaronas/delete">
                 {{csrf_field()}}
                 <input type="hidden" name="deleteID" value="{{$user->id}}">
                 <button class="btn btn-danger"  type="submit">Hapus</button>
               </form>
             </td>

           </tr>
           @endforeach
        </tbody>
        </table>
      </div>
      </div>
      </div>
      {{$hasil2->links()}}
      </div>
    </div>

  </div><!-- /.widget-box -->
</div><!-- /.row -->

<!-- <script src="js/tablefilter/tablefilter.js"></script>
<script data-config>
var filtersConfig = {
        base_path: 'js/tablefilter/',
        auto_filter: {
            delay: 1100 //milliseconds
        },
        col_3: 'select',
        col_2: 'select',
        col_6: 'select',
        col_7: 'select',
        col_8: 'select',

        responsive: true,
      //   grid_layout: {
      //     width: '100%'
      // },
      filters_cell_tag: 'th',

        // allows Bootstrap table styling
        themes: [{
            name: 'transparent'
        }],
        filters_row_index: 1,
        state: true,
        alternate_rows: true,
        // rows_counter: true,
        // btn_reset: true,
        // status_bar: true,
        msg_filter: 'Filtering...'
    };
    var tf = new TableFilter('IDtable', filtersConfig);
    tf.init();
</script> -->

@if(session()->has('message'))
<script>alert(' {{ session()->get('message') }}')</script>
@endif

@endsection
