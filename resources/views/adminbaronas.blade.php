@extends('layouts.admin')

@section('main-content')
<div class="row">
  <div class="">
    <div class="">
      <h5 class=""><i class="ace-icon fa fa-floppy-o green"></i> <b>ADMIN BARONAS</b></h5>
    </div>
    <div class="">
      <div class="">
      <div class="row">
      <div class="col-xs-12">
        <!-- <a style="margin-bottom:4px;" class="btn btn-primary btn-white btn-round" href="/tambahpeserta_e"><i class="ace-icon fa fa-plus-circle bigger-100"></i>Tambah Peserta</a>
        <form action="/search" method="get">
    <input type="text" name="cari" class="form-group" id="search" placeholder="Search">

  <button type="submit"  class="btn btn-white btn-info btn-bold">
    <i class="ace-icon fa fa-search bigger-110"></i>
    Cari Data
  </button>
</form>
<p style="color:blue;">DATA yang bisa dicari : Nama Team, Region, Email, Asal Sekolah, Tipetes, Alamat Sekolah. Bila nambah data yg dicari bilang aja</p>
        <div style="margin-top:4px;"> -->

        <table id="aa" name="table" class="table table-fixed table-striped table-bordered " cellspacing="0" width="100%">
        <thead>
          <tr>
            <th class="center">No</th>
            <th class="center">Bukti</th>
            <th class="center">cheklist</th>
            <th class="center">Nama Tim</th>
            <th class="center">No Peserta</th>
            <th class="center">Email</th>
            <th class="center">CP</th>
            <th class="center">Nama Pembimbing</th>
            <th class="center">Asal Sekolah</th>
            <th class="center">Alamat Sekolah</th>
            <th class="center">Nama Peserta1</th>
            <th class="center">Foto Peserta1</th>
            <th class="center">Kartu Pelajar1</th>
            <th class="center">Nama Peserta2</th>
            <th class="center">Foto Peserta2</th>
            <th class="center">Kartu Pelajar2</th>
            <th class="center">Nama Peserta3</th>
            <th class="center">Foto Peserta3</th>
            <th class="center">Kartu Pelajar3</th>


              <th class="center">Hapus</th>
          </tr>
        </thead>
        <tbody>
          <?php $no=1; ?>

          @foreach($jumlahuser2 as $user)
           <tr class="tablecontent">


             <td> {{ (($_GET['page']-1)*25)+$no }}</td>
              <?php $no++; ?>

             <td>    <a href="nope/gambar/buktibaronas/{{$user->bukti}}" target="_blank">{{$user->bukti}}</a></td>
             <td>

               <form method="post" action="/updatecheklist">
                 {{csrf_field()}}
                 <input type="hidden" name="updateID" value="{{$user->id}}"/>
                 <div class="col-md-6 checkbox">
                      <input type="hidden" name="kwitansi" value="false" />
                      @If ($user->kwitansi == "true")
                      <input type="checkbox" class="checkbox" name="kwitansi" checked="checked" id="kwitansi" value="true">Kwitansi</input>
                      @else
                      <input type="checkbox" class="checkbox" name="kwitansi" id="kwitansi" value="true">Kwitansi</input>
                      @endif
                      <input type="hidden" name="b_pendaftaran" value="false" />
                      @If ($user->b_pendaftaran == "true")
                      <input type="checkbox" class="checkbox" name="b_pendaftaran" id="b_pendaftaran" checked="checked" value="true">b_pendaftaran</input>
                      @else
                      <input type="checkbox" class="checkbox" name="b_pendaftaran" id="b_pendaftaran" value="true">b_pendaftaran</input>
                      @endif
                      <button type="submit" class="btn btn-success">Update</button>
                </div>

                </form>



             </td>
             <td>{{$user->name}}</td>
             <td>{{$user->no_peserta}}</td>
             <td>{{$user->email}}</td>
             <td>{{$user->notelp}}</td>
              <td>{{$user->b_namapembimbing}}</td>
             <td>{{$user->asalsekolah}}</td>
             <td>{{$user->alamatsekolah}}</td>
             <td>{{$user->b_namapeserta1}}</td>
             <td><a href="nope/gambar/b_foto/{{$user->b_fotopeserta1}}" target="_blank">{{$user->b_fotopeserta1}}</a></td>
             <td><a href="nope/gambar/b_kartupelajar/{{$user->b_ktppeserta1}}" target="_blank">{{$user->b_ktppeserta1}}</a></td>
             <td>{{$user->b_namapeserta2}}</td>
             <td><a href="nope/gambar/b_foto/{{$user->b_fotopeserta2}}" target="_blank">{{$user->b_fotopeserta2}}</a></td>
             <td><a href="nope/gambar/b_kartupelajar/{{$user->b_ktppeserta2}}" target="_blank">{{$user->b_ktppeserta2}}</a></td>
             <td>{{$user->b_namapeserta3}}</td>
             <td><a href="nope/gambar/b_foto/{{$user->b_fotopeserta3}}" target="_blank">{{$user->b_fotopeserta3}}</a></td>
             <td><a href="nope/gambar/b_kartupelajar/{{$user->b_ktppeserta3}}" target="_blank">{{$user->b_ktppeserta3}}</a></td>

             <td>
               <form method="post" action="/adminelectra/delete">
                 {{csrf_field()}}
                 <input type="hidden" name="deleteID" value="{{$user->id}}">
                 <button class="btn btn-danger"  type="submit">Hapus</button>
               </form>
             </td>

           </tr>
           @endforeach
        </tbody>
        </table>
      </div>
      </div>
      </div>
      {{$jumlahuser2->links()}}
      </div>
    </div>

  </div><!-- /.widget-box -->
</div><!-- /.row -->

<!-- <script src="js/tablefilter/tablefilter.js"></script>
<script data-config>
var filtersConfig = {
        base_path: 'js/tablefilter/',
        auto_filter: {
            delay: 1100 //milliseconds
        },
        col_3: 'select',
        col_2: 'select',
        col_6: 'select',
        col_7: 'select',
        col_8: 'select',

        responsive: true,
      //   grid_layout: {
      //     width: '100%'
      // },
      filters_cell_tag: 'th',

        // allows Bootstrap table styling
        themes: [{
            name: 'transparent'
        }],
        filters_row_index: 1,
        state: true,
        alternate_rows: true,
        // rows_counter: true,
        // btn_reset: true,
        // status_bar: true,
        msg_filter: 'Filtering...'
    };
    var tf = new TableFilter('IDtable', filtersConfig);
    tf.init();
</script> -->

@if(session()->has('message'))
<script>alert(' {{ session()->get('message') }}')</script>
@endif

@endsection
