@extends('layouts.admin')

@section('main-content')
@if(session()->has('message'))
<script>alert(' {{ session()->get('message') }}')</script>
@endif
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('INPUT PESERTA ELECTRA') }}</div>

                <div class="card-body">
                    <form id="daftar_electra" method="post" action="/peserta_e_tambah">
                        @csrf


                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('No peserta') }}</label>

                            <div class="col-md-6">
                                <input id="no_peserta" type="text" class="form-control{{ $errors->has('no_peserta') ? ' is-invalid' : '' }}" name="no_peserta" value="{{ old('no_peserta') }}" autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('no_peserta') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Nama Tim') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="region" class="col-md-4 col-form-label text-md-right">{{ __('Region') }}</label>

                            <div class="col-md-6">
                                  <div class="input-group">
                                  <select id="region" name="region" placeholder="Pilih Region" class="form-control{{ $errors->has('region') ? ' is-invalid' : '' }}">

                                    <option value="Online" @if (old('online') == "Online") selected @endif>Online</option>
                                    <option value="Surabaya" @if (old('surabaya') == "surabaya") selected @endif>Surabaya</option>
                                    <option value="Gresik" @if (old('gresik') == "gresik") selected @endif>Gresik</option>
                                    <option value="Mojokerto" @if (old('mojokerto') == "Mojokerto") selected @endif>Mojokerto</option>
                                    <option value="Malang" @if (old('malang') == "malang") selected @endif>Malang</option>
                                    <option value="Tuban" @if (old('tuban') == "tuban") selected @endif>Tuban</option>
                                    <option value="Probolinggo" @if (old('probolinggo') == "probolinggo") selected @endif>Probolinggo</option>
                                    <option value="Kediri" @if (old('kediri') == "kediri") selected @endif>Kediri</option>
                                    <option value="Madiun" @if (old('madiun') == "madiun") selected @endif>Madiun</option>
                                    <option value="Jember" @if (old('jember') == "jember") selected @endif>Jember</option>
                                    <option value="Banyuwangi" @if (old('banyuwangi') == "banyuwangi") selected @endif>Banyuwangi</option>
                                    <option value="Madura" @if (old('madura') == "madura") selected @endif>Madura</option>
                                    <option value="Solo" @if (old('solo') == "solo") selected @endif>Solo</option>
                                    <option value="Semarang" @if (old('semarang') == "semarang") selected @endif>Semarang</option>
                                    <option value="Jabodetabek" @if (old('jabodetabek') == "jabodetabek") selected @endif>Jabodetabek</option>
                                    <option value="Bali" @if (old('bali') == "bali") selected @endif>Bali</option>
                                    <option value="Balikpapan" @if (old('balikpapan') == "Balikpapan") selected @endif>Balikpapan</option>

                                  </select>
                                </div>
                            </div>
                            @if ($errors->has('region'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('region') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div id="tipetes-1" class="form-group row">
                            <label for="tipetes" class="col-md-4 col-form-label text-md-right">{{ __('Jenis Tes') }}</label>

                            <div class="col-md-6">
                              <div class="input-group">
                                  <select id="tipetes-2" name="tipetes" placeholder="Jenis Tes" class="form-control{{ $errors->has('tipetes') ? ' is-invalid' : '' }}">
                                    <option value="Offline">Offline</option>
                                    <option value="Online">Online</option>

                                  </select>
                              </div>
                                @if ($errors->has('tipetes'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('tipetes') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group" id="regsby">
                          <div class="col-md-12">
                          <a>Otomatis terdaftar untuk Tes Offline.</a>
                        </div>
                          </div>




                            <div class="form-group row">
                                <label for="asalsekolah" class="col-md-4 col-form-label text-md-right">{{ __('Asal sekolah') }}</label>

                                <div class="col-md-6">
                                    <input id="asalsekolah" type="text" class="form-control{{ $errors->has('asalsekolah') ? ' is-invalid' : '' }}" name="asalsekolah" value="{{ old('asalsekolah') }}" required>

                                    @if ($errors->has('asalsekolah'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('asalsekolah') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                        <div class="form-group row">
                            <label for="alamatsekolah" class="col-md-4 col-form-label text-md-right">{{ __('Alamat sekolah') }}</label>

                            <div class="col-md-6">
                                <input id="alamatsekolah" type="text" class="form-control{{ $errors->has('alamatsekolah') ? ' is-invalid' : '' }}" name="alamatsekolah" value="{{ old('alamatsekolah') }}" required>

                                @if ($errors->has('alamatsekolah'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('alamatsekolah') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="notelp" class="col-md-4 col-form-label text-md-right">{{ __('No Telp') }}</label>

                            <div class="col-md-6">
                                <input id="notelp" type="text" class="form-control{{ $errors->has('notelp') ? ' is-invalid' : '' }}" name="notelp" value="{{ old('notelp') }}" required>

                                @if ($errors->has('notelp'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('notelp') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="namaketua" class="col-md-4 col-form-label text-md-right">{{ __('Nama Ketua') }}</label>

                            <div class="col-md-6">
                                <input id="namaketua" type="text" class="form-control{{ $errors->has('namaketua') ? ' is-invalid' : '' }}" name="namaketua" value="{{ old('namaketua') }}" required>

                                @if ($errors->has('namaketua'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('namaketua') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div  class="form-group row">
                            <label for="kelasketua" class="col-md-4 col-form-label text-md-right">{{ __('Kelas Ketua') }}</label>

                            <div class="col-md-6">
                              <div class="input-group">
                                <select id="kelasketua" name="kelasketua" placeholder="Pilih Kelas Ketua" class="form-control">
                                  <option value="10" @if (old('10') == "10") selected @endif>10</option>
                                  <option value="11" @if (old('11') == "11") selected @endif>11</option>
                                  <option value="12" @if (old('12') == "12") selected @endif>12</option>
                                </select>
                              </div>
                                @if ($errors->has('kelasketua'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('kelasketua') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="namaanggota1" class="col-md-4 col-form-label text-md-right">{{ __('Nama Anggota1') }}</label>

                            <div class="col-md-6">
                                <input id="namaanggota1" type="text" class="form-control{{ $errors->has('namaanggota1') ? ' is-invalid' : '' }}" name="namaanggota1" value="{{ old('namaanggota1') }}">

                                @if ($errors->has('namaanggota1'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('namaanggota1') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div  class="form-group row">
                            <label for="kelasanggota1" class="col-md-4 col-form-label text-md-right">{{ __('Kelas Anggota 1') }}</label>

                            <div class="col-md-6">
                              <div class="input-group">
                                <select id="kelasanggota1" name="kelasanggota1" placeholder="Pilih Kelas Anggota 1" class="form-control">
                                  <option>Pilih kelas anggota 1</option>
                                  <option value="10" @if (old('10') == "10") selected @endif>10</option>
                                  <option value="11" @if (old('11') == "11") selected @endif>11</option>
                                  <option value="12" @if (old('12') == "12") selected @endif>12</option>
                                </select>
                              </div>
                                @if ($errors->has('kelasanggota1'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('kelasanggota1') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>





                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>



                        <input id="tipedaftar" type="hidden" name="tipedaftar" value="Offline"></input>
                        <input id="event" type="hidden" name="event" value="electra"></input>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary" onclick="inputhidden();">
                                    TAMBAH
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="js/input2.js"></script>
<script src="js/otomatisregion2.js"></script>
@endsection
