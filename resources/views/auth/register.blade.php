@extends('layouts.appreg')

@section('content')
<div id="particles-js" style="z-index:-5;"></div>
<div>
<div class="container">
        <div class="text-center main-info" style="margin:40px;" ><h2>Pilih Event</h2></div>
    </div>

    <div class="d-flex justify-content-between">

        <a href="/register/electra" style="max-width:30%;margin:auto;" >
            <img src="{{asset('assets/img/logoelectra.png')}}" class="img-fluid" ></img>
        </a>
        <a href="/register/baronas" style="max-width:30%;margin:auto;" >
            <img src="{{asset('assets/img/logobaronas.png')}}" class="img-fluid" ></img>
        </a>

    </div>

</div>


@endsection
