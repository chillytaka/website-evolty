@extends('layouts.master')


@section('title', 'Electra 8')

@section('nav-item')
<li class="nav-item">
  <a class="nav-link js-scroll-trigger" href="#page-top">HOME</a>
</li>
<li class="nav-item">
  <a class="nav-link js-scroll-trigger" href="#section1">ABOUT US</a>
</li>
<li class="nav-item">
  <a class="nav-link js-scroll-trigger" href="#section2">SYARAT</a>

</li>
<li class="nav-item">
  <a class="nav-link js-scroll-trigger" href="#section3">HADIAH</a>
</li>
<li class="nav-item">
  <a class="nav-link js-scroll-trigger" href="#section4">REGION</a>
</li>
<li class="nav-item">
  <a class="nav-link js-scroll-trigger" href="/electra/login">SIGN IN</a>
</li>
<li class="nav-item">
  <a class="nav-link js-scroll-trigger" href="/electra/register">REGISTER</a>
</li>
@endsection

@section('navbar-style')
style="background-color: #063a60;"
@endsection

@section('image-header')
<img src="http://evolty-its.com/native/img/electra123.png" style="height: 50px">
@endsection

@section('section1')
<div id="particles-js"></div>
@endsection

@section('footer')

<div class="col-sm-3" style="margin-top: 5vh">
  <img src="http://evolty-its.com/native/img/ns/logo_baru_bawah.png" class="img-fluid" style="height: 70px"><br><br>
    <div class="row">
        <img src="http://evolty-its.com/native/img/electra123.png" class="img-fluid" style="height: 60px">
        <img src="http://evolty-its.com/native/img/baronas1234.png" class="img-fluid" style="height: 60px">
        <img src="http://evolty-its.com/native/img/ns123.png" class="img-fluid" style="height: 60px">

        <img src="http://evolty-its.com/native/img/mage123.png" class="img-fluid" style="height: 60px">
    </div>
  </div>
<div class="col-sm-3" style="margin-top: 5vh">
  <div class="row">

  </div>
</div>
<div class="col-sm-6" style="margin-top: 5vh">
  <h5 style="font-size: 2em"><i class="material-icons">location_on</i>Event Of Electrical Faculty</h5>
  <table style="margin-top: 5vh">
    <tr>
      <td style="padding-right: 15px">Head Office </td>
      <td style="padding-right: 15px"> : 8211 Electrical Engineer Dept. FTE ITS</td>
    </tr>
    <tr>
      <td style="padding-right: 15px">Contact Person </td>
      <td style="padding-right: 15px"> : 085645114530</td>
    </tr>
    <tr>
      <td style="padding-right: 15px">Email </td>
      <td style="padding-right: 15px"> : event.himatektro@gmail.com</td>
    </tr>
  </table>
</div>
@endsection
