<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>BARONAS 2019</title>

     <link rel="stylesheet" href="{{url('css/style.css')}}"> <!-- Resource style -->

    <meta name="viewport" content="width=device-width,initial-scale=1.0"
    <meta property="og:image" content="{{url('assets/img/logobaronas.png')}}">
    <meta itemprop="image" content="{{url('assets/img/logobaronas.png')}}">
    <link rel="shortcut icon" href="{{asset('assets/img/logobaronas.png')}}" />


    <!-- Bootstrap core CSS -->
    <link href="{{url('vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">


    <!-- Custom styles for this template -->
    <link href="{{url('css/scrolling-nav.css')}}" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="{{url('css/mouse-anim.css')}}">
    <link href='https://fonts.googleapis.com/css?family=Ubuntu:300,400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="{{url('css/fullpage.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{url('css/baronas.css')}}" />
    <script src="{{url('js/modernizr.js')}}"></script> <!-- Modernizr -->
    <style>

    </style>
  </head>
  <body>
    <nav class="navbar navbar-expand-lg fixed-top " style="background-color: #1B1B1B;" >
      <div class="container">
        <a class="navbar-brand" href="#1"><img src="{{asset('assets/img/evolty.png')}}" style="height: 50px"></a>

        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#baronasNavBar">
          <span class="navbar-toggler-icon"><i class="material-icons" style="color:white">menu</i></span>
        </button>

        <div class="collapse navbar-collapse" id="baronasNavBar">
          <ul class="nav navbar-nav ml-auto" id="menu">
              <a data-menuanchor="1" class="nav-link" href="#1">HOME</a>
              <a data-menuanchor="3" class="nav-link" href="#3">KATEGORI</a>
              <a data-menuanchor="4" class="nav-link" href="#4">TIMELINE</a>
              <a data-menuanchor="7" class="nav-link" href="#7">GALLERY</a>
              <a data-menuanchor="6" class="nav-link" href="#6">CONTACT</a>
          </ul>
        </div>
      </div>
    </nav>

    <div id="fullpage">

    	<div class="section" id="section5" data-anchor="1" style="text-align:left;">
        <div id="particles-js">
        </div>
        <div class="icon-baronas" style="margin-top:10vh;" >
          <img src="{{asset('assets/img/logobaronas.png')}}" class="logobaronas" >
        </div>
        <div class="text-baronas" >
          <h1 class="title-baronas" >BARONAS</h1>
          <h2 class="text" >lomba robot nasional</h2>
          <h2 class="text"  >2019</h2>
        </div>
        <div style="text-align:center;margin-right:5%">
          <img src="https://www.evoltyits.com/image/baronas/berderet.png" >
        </div>
      </div>

      <div class="section" id="section1" data-anchor="8" >
        <h1 class="text kategori" >Hasil Kualifikasi</h1>
        <a href="/hasilSD" style="color:white">
          <div class="btn btn-lg btn-success">
            SD
          </div>
        </a>
        <a href="/hasilSMP" style="color:white">
          <div class="btn btn-lg btn-success">
            SMP
          </div>
        </a>
        <a href="/hasilSMA" style="color:white">
          <div class="btn btn-lg btn-success">
            SMA
          </div>
        </a>
      </div>

      <div class="section" id="section1" data-anchor="2" >
        <h3 style="vertical-align: center;color:white;font-size:3vh;" class="container">
          Baronas merupakan kompetisi robot</br> skala nasional yang diselenggarakan</br> setiap tahun oleh Fakultas Teknologi Elektro.</br> Dengan mengusung tema </br>"Transformasi Teknologi untuk Pelestarian Budaya Indonesia",</br> Baronas mempunyai misi yaitu melestarikan budaya Indonesia </br>di tengah kemajuan teknologi yang semakin pesat.
        </h3>
        <a href="/gbbaronas" style="color: white">
           <div class="btn btn-success">
             GuideBook Baronas
           </div>
        </a>
      </div>

      <div class="section vertical-scrolling" id="section2" data-anchor="3" >
        <div class="slide">
          <h1 class="text kategori" >KATEGORI</h1>
          <div class="container" >
            <div class="d-flex justify-content-around">
              <a class="border myborder border-white" href="baronas#3/sd">
                <h2 class="text judul-baronas " >ELEMENTARY</h2>
                <h3 class="text details-baronas ">PELESTARIAN BATIK</h3>
              </a>
              <a class="myborder border border-white" href="baronas#3/smp" >
                <h2 class="text judul-baronas">JUNIOR</h2>
                <h3 class="text details-baronas ">PELESTARIAN SENJATA TRADISIONAL</h3>
              </a>
            </div>
            <div class="d-flex justify-content-around separator" >
              <a class="myborder border border-white" href="baronas#3/sma" >
                  <h2 class="text judul-baronas">SENIOR</h2>
                  <h3 class="text details-baronas">PELESTARIAN RUMAH ADAT</h3>
              </a>
              <a class="myborder border border-white" href="baronas#3/open">
                <h2 class="text judul-baronas">OPEN</h2>
                <h3 class="text details-baronas ">SUMO ROBOT</h3>
              </a>
            </div>
          </div>
        </div>
        <div class="slide horizontal-scrolling" data-anchor="sd">
          <h1 class="text kategori" >KATEGORI</h1>
          <div class="kategori-title">
            <h1 class="text judul-baronas" >ELEMENTARY</h1>
            <h2 class="text judul-baronas" >PELESTARIAN BATIK</h2>
            <a class="btn btn-success" name="button" href="https://intip.in/RulesElementary">View Rules</a>
          </div>
          <div class="border border-right kategori-line"></div>
          <div class="kategori-info" >
            Dengan mengambil tema pelestarian batik, peserta menggunakan robot line follower analog untuk memproduksi sebuah kain batik berkualitas
          </div>
        </div>
        <div class="slide horizontal-scrolling" data-anchor="smp">
          <h1 class="text kategori" >KATEGORI</h1>
          <div class="kategori-title">
            <h1 class="text judul-baronas" >JUNIOR</h1>
            <h2 class="text judul-baronas" >PELESTARIAN SENJATA TRADISIONAL</h2>
            <a class="btn btn-success" name="button" href="https://intip.in/RulesJunior">View Rules</a>
          </div>
          <div class="border border-right kategori-line"></div>
          <div class="kategori-info desktop">
            Dengan mengambil tema pelestarian senjata tradisional, peserta menggunakan robot line tracer analog dan robot transporter. Robot transpoter merupakan representasi dari seorang pendekar yang ingin menempati takhta  kerajaan. Untuk mendapatkannya, pendekar harus membuat senjata tradisional yang memiliki kekuatan super
          </div>
          <div class="kategori-info mobile">
            Dengan mengambil tema pelestarian senjata tradisional, peserta menggunakan robot line tracer analog dan robot transporter untuk membuat senjata.
          </div>
        </div>
        <div class="slide horizontal-scrolling" data-anchor="sma">
          <h1 class="text kategori" >KATEGORI</h1>
          <div class="kategori-title">
            <h1 class="text judul-baronas" >SENIOR</h1>
            <h2 class="text judul-baronas" >PELESTARIAN RUMAH ADAT</h2>
            <a href="https://intip.in/RulesSenior" class="btn btn-success" name="button">View Rules</a>
          </div>
          <div class="border border-right kategori-line"></div>
          <div class="kategori-info" >
            Dengan mengambil tema pelestarian rumah adat, peserta menggunakan robot line tracer micro dan robot transporter untuk membangun rumah adat layak huni.
          </div>
        </div>
        <div class="slide horizontal-scrolling" data-anchor="open">
          <h1 class="text kategori" >KATEGORI</h1>
          <div class="kategori-title">
            <h1 class="text judul-baronas" >OPEN</h1>
            <h2 class="text judul-baronas" >SUMO ROBOT</h2>
            <a href="https://intip.in/RulesOpen" class="btn btn-success" name="button">View Rules</a>
          </div>
          <div class="border border-right kategori-line"></div>
          <div class="kategori-info desktop" >
            Kompetisi robot sumo manual yang dapat mendorong robot lawan keluar arena sesuai dengan peraturan pertandingan. Operator robot mengendalikan robot sumo menggunakan kontroler tanpa kabel/nirkabel.
          </div>
          <div class="kategori-info mobile" >
            Kompetisi robot sumo manual yang mendorong robot lawan keluar arena sesuai peraturan pertandingan. robot dikendalikan dengan kontroller nirkabel.
          </div>
        </div>
      </div>

      <div class="section" id="section5" data-anchor="4">
        <h1 style="vertical-align: top;color:white;padding-top:10vh;">TIMELINE</h1>
        <div class="container">
      	<div class="row">

      		<div class="col-md-12 timeline ">
      			<section class="main-timeline-section">
      			    <div class="timeline-start"></div>
      			    <div class="conference-center-line"></div>
      			    <div class="conference-timeline-content">

                  <div class="timeline-article timeline-article-bottom">
                      <div class="content-date" style="color:white;">
                          <span>23 - 24 MARET 2019</span>
                      </div>
                      <div class="meta-date"></div>
                      <div class="content-box">
                        <p>BARONAS lomba robot nasional 2019</p>
                      </div>
                  </div>

      			      <div class="timeline-article timeline-article-top">
      				        <div class="content-date" style="color:white;">
      			          		<span>22 MARET 2019</span>
      				        </div>
      				        <div class="meta-date"></div>
      				        <div class="content-box">
      				        	<p>TECHNICAL MEETING</p>
      				        </div>
      				    </div>

                 <div class="timeline-article timeline-article-bottom">
                     <div class="content-date" style="color:white;">
                         <span>14 FEB-9 MAR 2019</span>
                     </div>
                     <div class="meta-date"></div>
                     <div class="content-box">
                       <p>PENDAFTARAN GELOMBANG 2</p>
                     </div>
                 </div>
                 <div class="timeline-article timeline-article-top">
                    <div class="content-date" style="color:white;">
                        <span>2 JAN-13 FEB 2019</span>
                    </div>
                    <div class="meta-date"></div>
                    <div class="content-box">
                      <p>PENDAFTARAN GELOMBANG 1</p>
                    </div>
                </div>
      			   	</div>
      			   	<div class="timeline-end"></div>
      		  	</section>
      		</div>
      	</div>
      </div>
      </div>

      <div class="section" id="section2" data-anchor="7" >
        <div class="container">
          <div id="carouselExampleIndicators" class="carousel slide carousel-fade" data-ride="carousel" data-interval="2000" data-pause=false >
            <h1 class="mobile text" style="position:relative;top:-10px;">GALLERY</h1>
            <div class="carousel-inner">
              <div class="carousel-item active">
                <img class="d-block w-100" src="https://evoltyits.com/image/baronas/gallery/1.JPG">
              </div>
              <div class="carousel-item">
                <img class="d-block w-100" src="https://evoltyits.com/image/baronas/gallery/2.JPG">
              </div>
              <div class="carousel-item">
                <img class="d-block w-100" src="https://evoltyits.com/image/baronas/gallery/3.JPG">
              </div>
              <div class="carousel-item">
                <img class="d-block w-100" src="https://evoltyits.com/image/baronas/gallery/4.JPG">
              </div>
              <div class="carousel-item">
                <img class="d-block w-100" src="https://evoltyits.com/image/baronas/gallery/5.JPG">
              </div>
              <div class="carousel-item">
                <img class="d-block w-100" src="https://evoltyits.com/image/baronas/gallery/6.JPG">
              </div>
              <div class="carousel-item">
                <img class="d-block w-100" src="https://evoltyits.com/image/baronas/gallery/7.JPG">
              </div>
              <div class="carousel-item">
                <img class="d-block w-100" src="https://evoltyits.com/image/baronas/gallery/8.JPG">
              </div>
              <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="5"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="6"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="7"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="8"></li>
              </ol>
              <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
            </div>

          </div>
        </div>
      </div>


      <div class="section" id="section3" data-anchor="5" >
        <div class="container" style="margin-top:15vh;" >
          <div class="text" style="margin-bottom:5%;font-size:3em;"  >REGISTER NOW</div>
          <h3 class="text" style="font-size:1.3em;" >CLICK THE BUTTON BELOW</h2>
          <a href="/register/baronas" class="btn btn-light btn-lg" style="width:200px;margin-bottom:5vh;margin-top:5vh;" role="button">REGISTER</a>
          <h2 class="text" style="font-size:1.3em;" > OR SCROLL DOWN FOR</h2>
          <h2 class="text" style="font-size:2em;" > OFFLINE REGISTRATION</h2>
        </div>
        <div class="mouse-icon" style="margin:5vh auto;" >
          <span class="mouse-wheel"></span>
        </div>
      </div>

      <div class="section" id="section4" style="color:white;" data-anchor="6" >
        <div class="container">
          <div class="row">
            <div class="col cp-icon" >
              <a href="http://line.me/ti/p/~@evolty_its">
                <i class="fab fa-line footer-icon" style="color:green" ></i>
              </a>
              <a href="https://www.instagram.com/evolty_its/">
                <i class="fab fa-instagram footer-icon" style="color:magenta" ></i>
              </a>
              <a href="https://twitter.com/evolty_its">
                <i class="fab fa-twitter footer-icon" style="color:lightblue" ></i>
              </a>
              <a href="#">
                <i class="fab fa-facebook footer-icon" style="color:darkblue" ></i>
              </a>
            evolty_its
              <div>
                <a href="#">
                  <i class="fab fa-facebook footer-icon" style="color:darkblue" ></i>
                </a>
                Baronas
              </div>
            </div>

            <div class="col cp" >
              <h3>CONTACT PERSON</h3>
              <h4>Nazila 		(082211160442)</h4>
              <h4>Revo			(085746658456)</h4>
              <h4>Almizan (0895379116726)</h4>
              <h4>(WA)</h4>
            </div>
          </div>
        </div>
      </div>

    <script src="{{url('js/particles.js')}}"></script>
    <script src="{{url('js/app.js')}}"></script>
    <script src="{{url('vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{url('vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <script type="text/javascript" src="js/fullpage.js"></script>
<script type="text/javascript">
    var myFullpage = new fullpage('#fullpage', {
        menu: '#menu',
        navigation: true,
        slidesNavigation: true,
        autoScrolling:true,
    });
</script>

  </body>
</html>
