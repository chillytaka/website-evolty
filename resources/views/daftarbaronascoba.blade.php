@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

            <div class="card">
                <div class="card-header">{{ __('Register Baronas') }}</div>

                <div class="card-body">
                    <form id="daftar_baronas" method="POST" action="/registerbaronas" aria-label="#" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Nama Tim') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div  class="form-group row">
                            <label for="kategory" class="col-md-4 col-form-label text-md-right">{{ __('Kategory') }}</label>

                            <div class="col-md-6">
                              <div class="input-group">
                                <select id="kategory" name="kategory" placeholder="Pilih Kategory" class="form-control">
                                  <option value="SD" @if (old('sd') == "SD") selected @endif>SD</option>
                                  <option value="SMP" @if (old('smp') == "SMP") selected @endif>SMP</option>
                                  <option value="SMA" @if (old('sma') == "SMA") selected @endif>SMA</option>
                                </select>
                              </div>
                                @if ($errors->has('kategory'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('kategory') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="cp" class="col-md-4 col-form-label text-md-right">{{ __('CP') }}</label>

                            <div class="col-md-6">
                                <input id="cp" type="text" class="form-control{{ $errors->has('cp') ? ' is-invalid' : '' }}" name="cp" value="{{ old('cp') }}" required>

                                @if ($errors->has('cp'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('cp') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>



                        <div class="form-group row">
                          <label class="col-md-4 col-form-label text-md-right">{{ __('Upload Bukti Pembayaran (Max size 2 MB)') }}</label>
                          <div class="col-md-6">
                            <input type="file" name="bukti" value="" required/>
                            <span class="text-danger">
                                    {{ $errors->first('bukti') }}
                            </span>
                        </div>

                          </div>

                        <input id="event" type="hidden" name="event" value="baronas"></input>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary" onclick="inputhidden();">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="js/inputbaronas.js"></script>
@endsection
