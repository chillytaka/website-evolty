@extends('layouts.admin')

@section('main-content')

<div class="row">
  <div class="">
    <div class="">
      <h5 class=""><i class="ace-icon fa fa-floppy-o green"></i> <b>ADMIN ELECTRA</b></h5>
    </div>
    <div class="">
      <div class="">
      <div class="row">
      <div class="col-xs-12">
        <a style="margin-bottom:4px;" class="btn btn-primary btn-white btn-round" href="/tambahpeserta_e"><i class="ace-icon fa fa-plus-circle bigger-100"></i>Tambah Peserta</a>
        <a style="margin-bottom:4px;" class="btn btn-primary btn-white btn-round" href="/adminelectra?page=1"><i class="ace-icon fa fa-plus-circle bigger-100"></i>Clear Sort</a>

        <form action="/search" method="get">
    <input type="text" name="cari" class="form-group" id="search" placeholder="Search">
<input type="hidden" name="page" id="page" value="1" >
  <button type="submit"  class="btn btn-white btn-info btn-bold">
    <i class="ace-icon fa fa-search bigger-110"></i>
    Cari Data
  </button>

</form>

<p style="color:blue;">DATA yang bisa dicari : Nama Team, Region, Email, Asal Sekolah, Tipetes, Alamat Sekolah. Bila nambah data yg dicari bilang aja</p>
          @if(count($hasil))
        <div style="margin-top:4px;">

        <table id="aa" name="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
          <tr>
            <th class="center">No</th>
            <th class="center">Bukti</th>
            <th class="center">Verif</th>
            <th class="center">Status</th>
            <th class="center">Nama Tim</th>

            <th class="center">No Peserta</th>
            <th class="center">Tipe Daftar</th>
            <th class="center">Tipe Tes</th>
            <th class="center">Region</th>
            <th class="center">Email</th>
            <th class="center">No Telp</th>
            <th class="center">Asal Sekolah</th>
            <th class="center">Alamat Sekolah</th>
            <th class="center">Nama Ketua</th>
            <th class="center">Kelas Ketua</th>
            <th class="center">Nama Anggota 1</th>
            <th class="center">Kelas Anggota 1</th>
            <th class="center">Waktu</th>

              <th class="center">Edit</th>
              <th class="center">Hapus</th>
          </tr>
        </thead>
        <tbody>
<?php $no=1 ?>

          @foreach($hasil as $user)
           <tr class="tablecontent">



             <td> {{ (($_GET['page']-1)*25)+$no }}</td>
              <?php $no++; ?>


             <td>    <a href="nope/gambar/rekap/{{$user->bukti}}" target="_blank">{{$user->bukti}}</a></td>


             <td>
           <form method="post" action="/adminelectra/verif">
             {{csrf_field()}}
                 <input type="hidden" name="verifID" value="{{$user->id}}"/>
                 @if($user->status == 0)
                 <button class="btn btn-primary" type="submit">
                   Verif
                 </button>
                 @else
                 <button class="btn btn-danger" type="submit">
                   Unverif
                 </button>
                 @endif
               </form>
             </td>

             @if($user->status == 0)
               <td>Nonaktif</td>
             @else
               <td>Aktif</td>
             @endif

             <td>{{$user->name}}</td>
             <td>{{$user->no_peserta}}</td>
             <td>{{$user->tipedaftar}}</td>
             <td>{{$user->tipetes}}</td>
             <td>{{$user->region}}</td>
             <td>{{$user->email}}</td>
             <td>{{$user->notelp}}</td>
             <td>{{$user->asalsekolah}}</td>
             <td>{{$user->alamatsekolah}}</td>
             <td>{{$user->namaketua}}</td>
             <td>{{$user->kelasketua}}</td>
             <td>{{$user->namaanggota1}}</td>
             <td>{{$user->kelasanggota1}}</td>
             <td>{{$user->waktu}}</td>

             <td>
               <button type="button" data-toggle="modal" data-target="#{{ $user->id }}" data-uid="{{$user->id}} " class="update btn btn-warning btn-sm">Edit</button>
                 <div id="{{ $user->id }}" class="modal fade" role="dialog">
                   <div class="modal-dialog">
                     <div class="modal-content">
                       <div class="modal-header">
                         <button type="button" class="close" data-dismiss="modal">×</button>
                         <h4 class="modal-title">Edit Data Peserta</h4>
                       </div>
                       <form method="post" action="/adminelectra/update">
                         {{csrf_field()}}
                           <div class="modal-body" style="overflow-y: auto; max-height: 400px">
                             <div class="form-group row add">
                               <label class="control-label col-sm-2"> Nomor Peserta :</label>
                               <div class="col-sm-9">
                               <input type="hidden" name="updateID" value="{{$user->id}}"/>
                               @if($user->no_peserta == NULL)
                               <input id="no_peserta" type="text" class="no_peserta  form-control" name="no_peserta" value="{{ old('no_peserta') }}" required>
                               @else
                               <input id="no_peserta" type="text" class="no_peserta  form-control" name="no_peserta" value="{{$user->no_peserta}}" required>
                               @endif
                             </div>
                             </div

                             <br>
                             <div class="form-group row add">
                               <label class="control-label col-sm-2">Tipe Tes :</label>
                               <div class="col-sm-9">
                               @if($user->tipetes == NULL)
                               <input id="tipetes" type="text" class="form-control col-lg-8" name="tipetes" placeholder="Tipe Tes" value="{{ old('tipetes') }}" required>
                               @else
                               <input id="tipetes" type="text" class="form-control col-lg-8" name="tipetes" placeholder="Tipe Tes" value="{{$user->tipetes}}" required>
                               @endif
                             </div></div>


                             <br>
                             <div class="form-group row add">
                               <label class="control-label col-sm-2">Nama Tim :</label>
                               <div class="col-sm-9">
                               @if($user->name == NULL)
                               <input id="name" type="text" class="form-control col-lg-8" name="name" placeholder="Nama Tim" value="{{ old('name') }}" required>
                               @else
                               <input id="name" type="text" class="form-control col-lg-8" name="name" placeholder="Nama Tim" value="{{$user->name}}" required>
                               @endif
                             </div></div>
                             <br>
                             <div class="form-group row add">
                               <label class="control-label col-sm-2">Email :</label>
                               <div class="col-sm-9">
                               @if($user->email == NULL)
                               <input id="email" type="text" class="form-control col-lg-8" name="email" placeholder="Email" value="{{ old('email') }}" required>
                               @else
                               <input id="email" type="text" class="form-control col-lg-8" name="email" placeholder="Email" value="{{$user->email}}" required>
                               @endif
                             </div></div>
                             <br>
                             <div class="form-group row add">
                               <label class="control-label col-sm-2">Region :</label>
                               <div class="col-sm-9">
                               @if($user->region == NULL)
                               <input id="region" type="text" class="form-control col-lg-8" name="region" placeholder="Region" value="{{ old('region') }}" required>
                               @else
                               <input id="region" type="text" class="form-control col-lg-8" name="region" placeholder="Region" value="{{$user->region}}" required>
                               @endif
                             </div></div>
                               <br>
                             <div class="form-group row add">
                               <label class="control-label col-sm-2">Nomor Telepon :</label>
                               <div class="col-sm-9">
                               @if($user->notelp == NULL)
                               <input id="notelp" type="text" class="form-control col-lg-8" name="notelp" placeholder="Nomor Telepon" value="{{ old('notelp') }}" required>
                               @else
                               <input id="notelp" type="text" class="form-control col-lg-8" name="notelp" placeholder="Nomor Telepon" value="{{$user->notelp}}" required>
                               @endif
                             </div></div>
                              <br>
                             <div class="form-group row add">
                               <label class="control-label col-sm-2">Asal Sekolah :</label>
                               <div class="col-sm-9">
                               @if($user->asalsekolah == NULL)
                               <input id="asalsekolah" type="text" class="form-control col-lg-8" name="asalsekolah" placeholder="Asal Sekolah" value="{{ old('asalsekolah') }}" required>
                               @else
                               <input id="asalsekolah" type="text" class="form-control col-lg-8" name="asalsekolah" placeholder="Asal Sekolah" value="{{$user->asalsekolah}}" required>
                               @endif
                             </div></div>
                             <br>
                             <div class="form-group row add">
                               <label class="control-label col-sm-2">Alamat Sekolah :</label>
                               <div class="col-sm-9">
                               @if($user->alamatsekolah == NULL)
                               <input id="alamatsekolah" type="text" class="form-control col-lg-8" name="alamatsekolah" placeholder="Alamat Sekolah" value="{{ old('alamatsekolah') }}" required>
                               @else
                               <input id="alamatsekolah" type="text" class="form-control col-lg-8" name="alamatsekolah" placeholder="Alamat Sekolah" value="{{$user->alamatsekolah}}" required>
                               @endif
                             </div></div>
                             <br>
                             <div class="form-group row add">
                               <label class="control-label col-sm-2">Nama Ketua :</label>
                               <div class="col-sm-9">
                               @if($user->namaketua == NULL)
                               <input id="namaketua" type="text" class="form-control col-lg-8" name="namaketua" placeholder="Nama Ketua" value="{{ old('namaketua') }}" required>
                               @else
                               <input id="namaketua" type="text" class="form-control col-lg-8" name="namaketua" placeholder="Nama Ketua" value="{{$user->namaketua}}" required>
                               @endif
                             </div></div>
                             <br>
                             <div class="form-group row add">
                               <label class="control-label col-sm-2">Kelas Ketua :</label>
                               <div class="col-sm-9">
                               @if($user->kelasketua == NULL)
                               <input id="kelasketua" type="text" class="form-control col-lg-8" name="kelasketua" placeholder="Kelas Ketua" value="{{ old('kelasketua') }}" required>
                               @else
                               <input id="kelasketua" type="text" class="form-control col-lg-8" name="kelasketua" placeholder="Kelas Ketua" value="{{$user->kelasketua}}" required>
                               @endif
                             </div></div>
                             <br>
                             <div class="form-group row add">
                               <label class="control-label col-sm-2">Nama Anggota 1 :</label>
                               <div class="col-sm-9">
                               @if($user->namaanggota1 == NULL)
                               <input id="namaanggota1" type="text" class="form-control col-lg-8" name="namaanggota1" placeholder="Nama Anggota 1" value="{{ old('namaanggota1') }}" >
                               @else
                               <input id="namaanggota1" type="text" class="form-control col-lg-8" name="namaanggota1" placeholder="Nama Anggota 1" value="{{$user->namaanggota1}}" >
                               @endif
                             </div></div>
                             <br>
                             <div class="form-group row add">
                               <label class="control-label col-sm-2">Kelas Anggota 1 :</label>
                               <div class="col-sm-9">
                               @if($user->kelasanggota1 == NULL)
                               <input id="kelasanggota1" type="text" class="form-control col-lg-8" name="kelasanggota1" placeholder="Kelas Anggota 1" value="{{ old('kelasanggota1') }}" >
                               @else
                               <input id="kelasanggota1" type="text" class="form-control col-lg-8" name="kelasanggota1" placeholder="Kelas Anggota 1" value="{{$user->kelasanggota1}}" >
                               @endif
                             </div></div>
                             <br>
                             <div class="form-group row add">
                               <label class="control-label col-sm-2">Nama Anggota 2 :</label>
                               <div class="col-sm-9">
                               @if($user->namaanggota2 == NULL)
                               <input id="namaanggota2" type="text" class="form-control col-lg-8" name="namaanggota2" placeholder="Nama Anggota 2" value="{{ old('namaanggota2') }}" >
                               @else
                               <input id="namaanggota2" type="text" class="form-control col-lg-8" name="namaanggota2" placeholder="Nama Anggota 2" value="{{$user->namaanggota2}}" >
                               @endif
                             </div></div>
                             <br>
                             <div class="form-group row add">
                               <label class="control-label col-sm-2">Kelas Anggota 2 :</label>
                               <div class="col-sm-9">
                               @if($user->kelasanggota2 == NULL)
                               <input id="kelasanggota2" type="text" class="form-control col-lg-8" name="kelasanggota2" placeholder="Kelas Anggota 2" value="{{ old('kelasanggota2') }}" >
                               @else
                               <input id="kelasanggota2" type="text" class="form-control col-lg-8" name="kelasanggota2" placeholder="Kelas Anggota 2" value="{{$user->kelasanggota2}}" >
                               @endif
                             </div></div>
                             <br>
                             <div class="form-group row add">
                              <label class="control-label col-sm-2">Waktu :</label>
                              <div class="col-sm-9">
                              @if($user->waktu == NULL)
                              <input id="waktu" type="text" class="form-control col-lg-8" name="waktu" placeholder="Waktu" value="{{ old('waktu') }}" >
                              @else
                              <input id="waktu" type="text" class="form-control col-lg-8" name="waktu" placeholder="Waktu" value="{{$user->waktu}}" >
                              @endif
                            </div></div>
                            <br>

                           <div class="modal-footer">
                             <button type="submit" class="btn btn-success">Masukkan Data Peserta</button>
                             <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                           </div>
                       </form>
                     </div>
                   </div>
                 </div>
             </td>

             <td>
               <form method="post" action="/adminelectra/delete">
                 {{csrf_field()}}
                 <input type="hidden" name="deleteID" value="{{$user->id}}">
                 <button class="btn btn-danger"  type="submit">Hapus</button>
               </form>
             </td>

           </tr>
           @endforeach
        </tbody>
        </table>
      </div>
      </div>
      </div>
      {{$hasil->links()}}
      </div>
    </div>

  </div><!-- /.widget-box -->
</div><!-- /.row -->

<!-- <script src="js/tablefilter/tablefilter.js"></script>
<script data-config>
var filtersConfig = {
        base_path: 'js/tablefilter/',
        auto_filter: {
            delay: 1100 //milliseconds
        },
        col_3: 'select',
        col_2: 'select',
        col_6: 'select',
        col_7: 'select',
        col_8: 'select',

        responsive: true,
      //   grid_layout: {
      //     width: '100%'
      // },
      filters_cell_tag: 'th',

        // allows Bootstrap table styling
        themes: [{
            name: 'transparent'
        }],
        filters_row_index: 1,
        state: true,
        alternate_rows: true,
        // rows_counter: true,
        // btn_reset: true,
        // status_bar: true,
        msg_filter: 'Filtering...'
    };
    var tf = new TableFilter('IDtable', filtersConfig);
    tf.init();
</script> -->
@else
<h1>DATA TIDAK DITEMUKAN</h>
@endif
@if(session()->has('message'))
<script>alert(' {{ session()->get('message') }}')</script>
@endif

@endsection
