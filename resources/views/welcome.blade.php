@extends('layouts.master')

@section('title', 'Evolty 2019')

@section('nav-item')
  <li class="nav-item">
    <a class="nav-link js-scroll-trigger link-text " href="#page-top">HOME</a>
  </li>
  <li class="nav-item">
    <a class="nav-link js-scroll-trigger" href="#section2">THEME</a>

  </li>
  <li class="nav-item">
    <a class="nav-link js-scroll-trigger" href="#section3">ELECTRA</a>
  </li>
  <li class="nav-item">
    <a class="nav-link js-scroll-trigger" href="#section4">BARONAS</a>
  </li>
  <li class="nav-item">
    <a class="nav-link js-scroll-trigger" href="#section5">MAGE4</a>
  </li>
@guest
  <li class="nav-item">
    <a class="nav-link js-scroll-trigger" href="/login">LOGIN</a>
  </li>
  <li class="nav-item">
    <a class="nav-link js-scroll-trigger" href="/register">REGISTER</a>
  </li>
@else
  <li class="nav-item">
    <a class="nav-link js-scroll-trigger" href="/home">DASHBOARD</a>
  </li>
  <li class="nav-item dropdown">
      <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
         {{ Auth::user()->name }} <span class="caret"></span>
      </a>

      <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="{{ route('logout') }}"
             onclick="event.preventDefault();
                           document.getElementById('logout-form').submit();">
              {{ __('Logout') }}
          </a>

          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              @csrf
          </form>
      </div>
  </li>
@endguest
@endsection

@section('navbar-style')
style="background-color: #1B1B1B;"
@endsection

@section('image-header')
  <img src="{{asset('assets/img/evolty.png')}}" style="height: 50px">
@endsection

@section('section1')


<div id="particles-js"></div>

  <div class="container" >

      <img src="{{asset('assets/img/evolty.png')}}" class="img-fluid" style="margin-left:25%;margin-right:auto;max-width:45%;" >
      <div class="container">
        <h4 class="text-center main-info desktop">
        EVOLTY 2019 adalah rangkaian event terbesar di ITS yang diselenggarakan oleh Fakultas Teknologi Elektro Institut Teknologi Sepuluh Nopember Surabaya untuk mewadahi dan meningkatkan atmosfir kompetitif pelajar se-Indonesia khususnya dalam rumpun ilmu elektro.
        </h4>
        <h4 class="text-center main-info mobile" >
        EVOLTY 2019 adalah rangkaian event terbesar di ITS yang diselenggarakan oleh Fakultas Teknologi Elektro ITS
        </h4>
    </div>
      <hr class="line main-info" />
  </div>
  <a class="link js-scroll-trigger" href="#section2">
    <h4 class="text-center footer-info">Click For More Info</h4>
  </a>
@endsection

@section('section2')
  <script type="text/javascript">
  $('.carousel').carousel({
      interval: 500
      })
  </script>
  <div class="container display-4">Theme</div>
  <div class="row no-gutters">
    <style>
    .art{width:60vh;height: auto;}
    .carousel{width:100%;height: auto;}
    </style>

    <div class="col-sm-6 align-self-center">
      <div class="d-flex justify-content-center">
        <blockquote class="blockquote text-center" style="width:80%;height:50%">
          <p class="mb-0"style="color:#5F6368;font-size:4vh;font-weight:bold">"Transformasi Budaya dan Teknologi untuk Menumbuhkan Kreativitas Bangsa"</p>
        </blockquote>
      </div>
    </div>

    <div class="col-sm-6">
      <div class="d-flex justify-content-center">
        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">

          <div class="carousel-inner">
              <div class="carousel-item active">
                <div class="my-card"><img class="art" src="https://www.evoltyits.com/assets/img/Artboard1.jpg"></div>
              </div>
              <div class="carousel-item">
                <div class="my-card"><img class="art" src="https://www.evoltyits.com/assets/img/Artboard2.jpg"></div>
              </div>
              <div class="carousel-item">
                <div class="my-card"><img class="art" src="https://www.evoltyits.com/assets/img/Artboard3.jpg"></div>
              </div>
          </div>
          <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true" style="color:#1B1B1B;"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true" style="color:#1B1B1B;"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
      </div>
    </div>
  </div>


@endsection

@section('section3')

<div id="about" class="row col-xl-12 col-lg-12 col-m-12 col-sm-12 about">
      <div class="col-xl-5 col-l-5 col-m-6 col-sm-12" style="">
         <img src="{{url('assets/img/logoelectra.png')}}" style="width:70%;">
      </div>
      <div class="col-xl-7 col-l-7 col-m-6 col-sm-12" style="text-align:justify;">
         <p style="color:white;">
            <b>Electra Competition 8</b> Merupakan Olimpiade keteknik-elektroan yang ditujukan kepada siswa/i SMA/MA/SMK sederajat di seluruh Indonesia.
            Electra Competition 8 hadir untuk memperkenalkan teknologi pintar kepada generasi-generasi muda, khususnya siswa-siswi SMA dan sederajat.
            Electra Competition 8 terdiri dari 3 tahap seleksi : Penyisihan, Semi Final dan Final.Tahap Penyisihan dilakukan melalui 2 bentuk, seleksi online dan offline yang akan dilakukan di 16 region.
            Semifinal dan Grand Final dilakukan di Surabaya.

           </p>
         <a href="http://bit.ly/GuideBookElectra8" style="color: white">
            <div class="btn btn-secondary">
              GuideBook Electra
            </div>
         </a>
         <a href="/register/electra" style="color: white">
            <div class="btn btn-secondary">
              Daftar Electra
            </div>
         </a>
         <a href="http://bit.ly/KoleksiSoalElectra8" target="_blank" style="color: white">
            <div class="btn btn-secondary">
            Arsip Soal
            </div>
         </a>

   </div>
@endsection

@section('section4')
<div  class="row col-xl-12 col-lg-12 col-m-12 col-sm-12 about">
  <div class="col-xl-5 col-l-5 col-m-6 col-sm-12" style="">
     <img src="{{url('assets/img/logobaronas.png')}}" style="width:70%;">
  </div>
      <div class="col-xl-7 col-l-7 col-m-6 col-sm-12" style="text-align:justify;">
         <p style="">
            <b>Baronas 2019</b> merupakan kompetisi robot skala nasional yang diselenggarakan setiap tahun oleh Fakultas Teknologi Elektro. Dengan mengusung tema "Transformasi Teknologi untuk Pelestarian Budaya Indonesia", Baronas mempunyai misi yaitu melestarikan budaya Indonesia di tengah kemajuan teknologi yang semakin pesat.
           </p>
         <a href="/register/baronas" style="color: white">
            <div class="btn btn-secondary">
              Register Baronas
            </div>
         </a>
         <a href="/baronas" style="color: white">
            <div class="btn btn-secondary">
              Visit Baronas
            </div>
         </a>
         <a href="/gbbaronas" style="color: white">
            <div class="btn btn-secondary">
              GuideBook Baronas
            </div>
         </a>
      </div>

   </div>
@endsection
@section('section5')
<div  class="row col-xl-12 col-lg-12 col-m-12 col-sm-12 about">
  <div class="col-xl-5 col-l-5 col-m-6 col-sm-12" style="">
     <img src="http://mage.telematics.its.ac.id/Image/mage4.png" style="width:75%;">
  </div>
      <div class="col-xl-7 col-l-7 col-m-6 col-sm-12" style="text-align:justify;">
         <p style="color:white;">
            <b>MAGE (Multimedia and Game Event)</b> merupakan serangkaian Kompetisi IT yang diadakan oleh Departemen Teknik Komputer Fakultas Teknologi Elektro ITS Surabaya sebagai media bagi pelajar dan akademisi dalam mengeksplorasi kreativitas, inovasi dan kemampuan dalam bidang IT seperti IoT, Game Dev, Apps Dev dan masih banyak lagi.  Departemen Teknik Komputer adalah salah satu Departemen yang berada dibawah naungan Fakultas Teknologi Elektro ITS. Departemen Teknik Komputer memiliki Akreditasi A. Adapun disiplin ilmu yaitu  mewujudkan ilmu pengetahuan dan teknologi dari desain, membangun, implementasi, dan pemeliharaan perangkat lunak dan perangkat keras dari sistem komputasi modern, peralatan dikontrol komputer, dan jaringan perangkat cerdas.
           </p>
         <a href="http://mage.telematics.its.ac.id" style="color: white">
            <div class="btn btn-secondary">
              Visit MAGE4
            </div>
         </a>
      </div>

   </div>
@endsection

@section('footer')
  <div class="container">
      <div class="row">
        <div class="col-sm-6" style="margin-top: 5vh">
                <table style="margin-top: 0vh">
                        <tr>
                            <td style="padding-left: 20px">
                                Temukan kami :
                            </td>
                        </tr>
                      <tr>
                        <td style="padding-right: 15px"><a class="nav-link js-scroll-trigger" href="http://line.me/ti/p/~@evolty_its">
                            <img src="https://img.icons8.com/color/1600/line-me.png" class="img-fluid" style="height: 40px;color:white"> @evolty_its </a></td>
                      </tr>
                    <tr>
                      <td style="padding-right: 15px">
                        <a class="nav-link js-scroll-trigger" href="https://www.instagram.com/evolty_its/">
                            <img src="{{url('assets/img/ig.png')}}" class="img-fluid" style="height: 37px"> evolty_its  </a></td>
                    </tr>

                    <tr>
                      <td style="padding-right: 15px">
                        <a class="nav-link js-scroll-trigger" href="https://twitter.com/evolty_its">
                            <img src="https://cdn2.iconfinder.com/data/icons/minimalism/512/twitter.png" class="img-fluid" style="height: 40px"> evolty_its </a></td>
                    </tr>
                    </table>
</div>

<div class="col-sm-6" style="margin-top: 5vh">
<h5 style="font-size: 2em; margin-left: 20px">Fakultas Teknologi Elektro</h5>
<h6 style="font-size: 1em">
    <a class="nav-link js-scroll-trigger" href="https://goo.gl/maps/eLhXk6EjHB72">
        <i class="material-icons">location_on</i> Gedung B, C & AJ Kampus ITS Sukolilo, Surabaya 60111</a>
      </h6>
      <h5 style="font-size: 2em; margin-left: 20px">Media Partner</h5>
      <h6 style="font-size: 1em">
          <a class="nav-link js-scroll-trigger" href="https://www.haievent.com/" title="HaiEvent.com"><img src="https://ucarecdn.com/3885c85e-ab26-4942-aa8f-a8522985b3ab/logo1pp.png" alt="Media Partner"></a>
            </h6>

</div>
      </div>
      <div class="row d-flex justify-content-center">
            <div>
                <table>
                    <tr>
                  <td><img src="{{url('assets/img/logoevolty.png')}}" class="img-fluid" style="height: 90px"></td>
                    <td><img src="{{url('assets/img/logoelectra.png')}}" class="img-fluid" style="height: 60px"></td>
                    <td><img src="{{url('assets/img/logobaronas.png')}}" class="img-fluid" style="height: 60px"></td>
                     </tr>
                </table>
            </div>
        </div>
    </div>





@endsection
