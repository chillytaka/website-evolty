<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>BARONAS 2019</title>
    <link rel="stylesheet" type="text/css" href="css/fullpage.css" />
    <link rel="stylesheet" type="text/css" href="css/baronas.css" />

    <link rel="stylesheet" href="{{url('css/style.css')}}"> <!-- Resource style -->


    <meta property="og:image" content="{{url('assets/img/logobaronas.png')}}">
    <meta itemprop="image" content="{{url('assets/img/logobaronas.png')}}">
    <link rel="shortcut icon" href="{{asset('assets/img/logobaronas.png')}}" />


    <!-- Bootstrap core CSS -->
    <link href="{{url('vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">


    <!-- Custom styles for this template -->
    <link href="{{url('css/scrolling-nav.css')}}" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="{{url('css/mouse-anim.css')}}">
    <link href='https://fonts.googleapis.com/css?family=Ubuntu:300,400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <script src="{{url('js/modernizr.js')}}"></script> <!-- Modernizr -->

    <style>
    #particles-js{
  position: absolute;
  width: 100%;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  overflow: hidden;

}

#fp-nav ul li a span,
.fp-slidesNav ul li a span {
    background: white;
    width: 8px;
    height: 8px;
    margin: -4px 0 0 -4px;
}

#fp-nav ul li a.active span,
.fp-slidesNav ul li a.active span,
#fp-nav ul li:hover a.active span,
.fp-slidesNav ul li:hover a.active span {
    width: 16px;
    height: 16px;
    margin: -8px 0 0 -8px;
    background: transparent;
    box-sizing: border-box;
    border: 1px solid #24221F;
}
    </style>
  </head>
  <body>

    <div id="fullpage">

    	<div class="section " id="section0" style="text-align:left;">
        <div id="particles-js">
        </div>
        <div style="display:inline-block;width:50%">
          <img src="{{asset('assets/img/logobaronas.png')}}" class="logobaronas" >
        </div>
        <div style="display:inline-block;vertical-align:middle" >
          <h1 style="font-size:10vh;color:white" >BARONAS</h1>
          <h2 class="text" >lomba robot nasional</h2>
          <h2 class="text"  >2019</h2>
        </div>
        <div style="width:auto;text-align:center;">
          <img src="image/baronas/berderet.png" >
        </div>
      </div>
      <div class="section" id="section1" >
        <h3 style="vertical-align: center;color:white">
          Baronas merupakan kompetisi robot</br> skala nasional yang diselenggarakan</br> setiap tahun oleh Fakultas Teknologi Elektro.</br> Dengan mengusung tema </br>"Transformasi Teknologi untuk Pelestarian Budaya Indonesia",</br> Baronas mempunyai misi yaitu melestarikan budaya Indonesia </br>di tengah kemajuan teknologi yang semakin pesat.
        </h3>
      </div>
      <div class="section vertical-scrolling" id="section2" >
        <div class="slide">
          <h1 class="text kategori" >KATEGORI</h1>
          <div class="container">
            <div class="d-flex justify-content-around">
              <a class="border border-white" style="padding:20px;width:40%" href="#sd">
                <h2 class="text">ELEMENTARY</h2>
                <h3 class="text">PELESTARIAN BATIK</h3>

              </a>
              <a class="border border-white" style="padding:20px;width:40%" href="#smp" >
                <h2 class="text">JUNIOR</h2>
                <h3 class="text">PELESTARIAN SENJATA TRADISIONAL</h3>
              </a>
            </div>
            <div class="d-flex justify-content-around" style="margin-top:40px;" >
              <a class="border border-white" style="padding:20px;width:40%" href="#sma" >
                  <h2 class="text">SENIOR</h2>
                  <h3 class="text">PELESTARIAN RUMAH ADAT</h3>
              </a>
              <a class="border border-white" style="padding:20px;width:40%">
                <h2 class="text">OPEN</h2>
                <h3 class="text">SUMO ROBOT</h3>
              </a>
            </div>
          </div>
        </div>
        <div class="slide horizontal-scrolling" id="sd">
          <h1 class="text kategori" >KATEGORI</h1>
          <div class="kategori-title">
            <h1 class="text" >ELEMENTARY</h1>
            <h2 class="text" >PELESTARIAN BATIK</h2>
            <button class="btn btn-success" name="button"><a target="_blank" href="https://intip.in/RulesElementary">View Rule</a></button>
          </div>
          <div class="border border-right kategori-line"></div>
          <div class="kategori-info" >
            Dengan mengambil tema pelestarian batik, peserta menggunakan robot line follower analog untuk memproduksi sebuah kain batik berkualitas
          </div>
        </div>




        <div class="slide horizontal-scrolling" id="smp">
          <h1 class="text kategori" >KATEGORI</h1>
          <div class="kategori-title">
            <h1 class="text" >JUNIOR</h1>
            <h2 class="text" >PELESTARIAN SENJATA TRADISIONAL</h2>
            <button class="btn btn-success" name="button"><a target="_blank" href="https://intip.in/RulesJunior">View Rule</a></button>
          </div>
          <div class="border border-right kategori-line"></div>
          <div class="kategori-info">
            Dengan mengambil tema pelestarian senjata tradisional, peserta menggunakan  robot line tracer analog dan robot transporter. Robot transpoter merupakan representasi dari seorang pendekar yang ingin menempati takhta  kerajaan. Untuk mendapatkannya, pendekar harus membuat senjata tradisional yang memiliki kekuatan super
          </div>
        </div>
        <div class="slide horizontal-scrolling" id="sma">
          <h1 class="text kategori" >KATEGORI</h1>
          <div class="kategori-title">
            <h1 class="text" >SENIOR</h1>
            <h2 class="text" >PELESTARIAN RUMAH ADAT</h2>
            <button class="btn btn-success" name="button"><a target="_blank" href="https://intip.in/RulesSenior">View Rule</a></button>
        </div>
          <div class="border border-right kategori-line"></div>
          <div class="kategori-info" >
            Dengan mengambil tema pelestarian rumah adat, peserta menggunakan robot line tracer micro dan robot transporter untuk membangun rumah adat layak huni.
          </div>
        </div>
        <div class="slide horizontal-scrolling" id="open">
          <h1 class="text kategori" >KATEGORI</h1>
          <div class="kategori-title">
            <h1 class="text" >OPEN</h1>
            <h2 class="text" >SUMO ROBOT</h2>
            <button class="btn btn-success" name="button"><a target="_blank" href="https://intip.in/RulesOpen">View Rule</a></button>
          </div>
          <div class="border border-right kategori-line"></div>
          <div class="kategori-info" >
            Kompetisi robot sumo manual yang dapat mendorong robot lawan keluar arena sesuai dengan peraturan pertandingan. Operator robot mengendalikan robot sumo menggunakan kontroler tanpa kabel/nirkabel.</div>
        </div>
      </div>
<div class="section" id="section5">
  <h1 style="vertical-align: top;color:white;">TIMELINE</h1>
  <div class="container">
	<div class="row">

		<div class="col-md-12 col-sm-12 col-xs-12" style="display:inline-block;vertical-align:middle">
			<section class="main-timeline-section">
			    <div class="timeline-start"></div>
			    <div class="conference-center-line"></div>
			    <div class="conference-timeline-content">

            <div class="timeline-article timeline-article-top">
                <div class="content-date" style="color:white;">
                    <span>23-24 MARET 2019</span>
                </div>
                <div class="meta-date"></div>
                <div class="content-box">
                  <p>BARONAS lomba robot nasional 2019</p>
                </div>
            </div>

			      	 <div class="timeline-article timeline-article-top">
				        <div class="content-date" style="color:white;">
			          		<span>22 MARET 2019</span>
				        </div>
				        <div class="meta-date"></div>
				        <div class="content-box">
				        	<p>TECHNICAL MEETING</p>
				        </div>
				    </div>

           <div class="timeline-article timeline-article-top">
               <div class="content-date" style="color:white;">
                   <span>14 FEB-9 MAR 19</span>
               </div>
               <div class="meta-date"></div>
               <div class="content-box">
                 <p>PENDAFTARAN GELOMBANG 2</p>
               </div>
           </div>
           <div class="timeline-article timeline-article-top">
              <div class="content-date" style="color:white;">
                  <span>2 JAN-13 FEB 19</span>
              </div>
              <div class="meta-date"></div>
              <div class="content-box">
                <p>PENDAFTARAN GELOMBANG 1</p>
              </div>
          </div>
			   	</div>
			   	<div class="timeline-end"></div>
		  	</section>
		</div>
	</div>
</div>
</div>


      <div class="section" id="section3" >
        <div class="container">
          <div class="display-4 text" style="margin-bottom:5%;"  >REGISTER NOW</div>
          <h3 class="text" >CLICK THE BUTTON BELOW</h2>
          <a href="/register/baronas" class="btn btn-light btn-lg" style="width:200px;margin-bottom:10vh;margin-top:10vh" role="button">REGISTER</a>
          <h2 class="text"> OR SCROLL DOWN FOR</h2>
          <h2 class="text"> OFFLINE REGISTRATION</h2>
        </div>
        <div class="mouse-icon" style="margin:50px auto;" >
          <span class="mouse-wheel"></span>
        </div>
      </div>
      <div class="section" id="section4" style="color:white;" >
        <div class="container">
          <div class="row">
          <div class="col" style="font-size:2em;margin-top:5em;" >
            <a href="http://line.me/ti/p/~@evolty_its">
              <i class="fab fa-line footer-icon" style="color:green" ></i>
            </a>
            <a href="https://www.instagram.com/evolty_its/">
              <i class="fab fa-instagram footer-icon" style="color:magenta" ></i>
            </a>
            <a href="https://twitter.com/evolty_its">
              <i class="fab fa-twitter footer-icon" style="color:lightblue" ></i>
            </a>
            <a href="#">
              <i class="fab fa-facebook footer-icon" style="color:darkblue" ></i>
            </a>
            evolty_its
            <div>
              <a href="#">
                <i class="fab fa-facebook footer-icon" style="color:darkblue" ></i>
              </a>
              Baronas
            </div>
          </div>

          <div class="col" style="vertical-align:middle;margin-top:8em" >
            <h3>CONTACT PERSON</h3>
            <h4>Nazila 		(082211160442)</h4>
            <h4>Revo			(085746658456)</h4>
            <h4>Almizan (0895379116726)</h4>
            <h4>(WA)</h4>
          </div>
      </div>
        </div>
      </div>

    	<!-- <div class="section" id="section1">
    	    <div class="slide" id="slide1"><h1>Slide Backgrounds</h1></div>
    	    <div class="slide" id="slide2"><h1>Totally customizable</h1></div>
    	</div>
    	<div class="section" id="section2"><h1>Lovely images <br />for a lovely page</h1></div>
    	<div class="section" id="section3"><h1>One Image = One thousand words</h1></div>-->
    </div>

    <script src="{{url('js/particles.js')}}"></script>
    <script src="{{url('js/app.js')}}"></script>
<script type="text/javascript" src="js/fullpage.js"></script>
<script type="text/javascript">
    var myFullpage = new fullpage('#fullpage', {

        navigation: true,
    slidesNavigation: true,
    });
</script>

  </body>
</html>
