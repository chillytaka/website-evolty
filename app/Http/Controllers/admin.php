<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;

class admin extends Controller
{


  public function index()
  {
      $jumlahuser = User::where('event','electra')->paginate(25);
      return view('adminelectra', compact('jumlahuser'));
  }

  public function indexbaronas()
  {
      $jumlahuser2 = User::where('event','baronas')->where('status','LIKE','1')->paginate(25);
      return view('adminbaronas', compact('jumlahuser2'));
  }

  public function update(Request $inputnya)
  {
    $IDupdate = $inputnya->input('updateID');
    $baru = User::find($IDupdate);
    $baru->no_peserta = $inputnya->input('no_peserta');
    $baru->tipetes = $inputnya->input('tipetes');
    $baru->name = $inputnya->input('name');
    $baru->email = $inputnya->input('email');
    $baru->region = $inputnya->input('region');
    $baru->notelp = $inputnya->input('notelp');
    $baru->asalsekolah = $inputnya->input('asalsekolah');
    $baru->alamatsekolah = $inputnya->input('alamatsekolah');
    $baru->namaketua = $inputnya->input('namaketua');
    $baru->kelasketua = $inputnya->input('kelasketua');
    $baru->namaanggota1 = $inputnya->input('namaanggota1');
    $baru->kelasanggota1 = $inputnya->input('kelasanggota1');

    $baru->save();
    return redirect()->back();
  }

  public function delete(Request $request){

    $IDdelete = $request->input('deleteID');
    // $delete = User::find($IDdelete);
    User::where('id', '=', $IDdelete)->delete();
    // $delete->save();
      return redirect()->back();
  }

  public function verifikasi(Request $request)
{
    $verif = User::find($request->verifID);
    // dd($verif);
      // $nopesertanew = User::find($request->nopesertanew);
    if($verif->status == 0){
      $verif->status = 1;
          // $verif->no_peserta = $data['nopesertanew'];
          $verif->update();
        return redirect()->back()->with('message', 'Akun Sudah Diaktifkan');
    }
    else {
      $verif->status = 0;
      $verif->update();
    return redirect()->back()->with('message', 'Akun Sudah Di Nonaktifkan');
    }

}

public function tambah(Request $inputnya)
{

  User::insert([
    ['name' => $inputnya->input('name'),
    'no_peserta' => $inputnya->input('no_peserta'),
    'region' => $inputnya->input('region'),
    'asalsekolah' => $inputnya->input('asalsekolah'),
    'alamatsekolah' => $inputnya->input('alamatsekolah'),
    'namaketua' => $inputnya->input('namaketua'),
    'kelasketua' => $inputnya->input('kelasketua'),
    'namaanggota1' => $inputnya->input('namaanggota1'),
    'kelasanggota1' => $inputnya->input('kelasanggota1'),
    'email' => $inputnya->input('email'),
    'notelp' => $inputnya->input('notelp'),
    'password' => bcrypt($inputnya->input('password')),
    'cadangan' => $inputnya->input('password'),
    'tipetes' => $inputnya->input('tipetes'),
    'tipedaftar' => $inputnya->input('tipedaftar'),
    'status' => '1',
    'event' => $inputnya->input('event') ]
  ]);
  return redirect()->back()->with('message', 'Berhasil Tambah');
}

public function search(Request $request)
{
  $search1 = $request->get('cari');
  $page = $request->get('page');
  $hasil = User::where('event','electra')
  ->where('region','LIKE','%'.$search1.'%')
  ->orwhere('name','LIKE','%'.$search1.'%')
  ->orwhere('email','LIKE','%'.$search1.'%')
  ->orwhere('asalsekolah','LIKE','%'.$search1.'%')
  ->orwhere('tipetes','LIKE','%'.$search1.'%')
  ->orwhere('alamatsekolah','LIKE','%'.$search1.'%')
  ->orwhere('namaketua','LIKE','%'.$search1.'%')
  ->orwhere('namaanggota1','LIKE','%'.$search1.'%')
  ->orwhere('no_peserta','LIKE','%'.$search1.'%')
  ->paginate(25);
  $hasil->appends($request->only('cari',"page"));
  return view('search',compact('search1','hasil','page'));
}
public function needverif(Request $request)
{

  $hasil2 = User::where('event','electra')->where('status','LIKE','0')
  ->paginate(25);
  return view('pesertabutuhverif',compact('hasil2'));
}
public function needpass(Request $request)
{

  $hasil2 = User::where('event','baronas')->where('status','LIKE','0')
  ->paginate(25);
  return view('pesertaneedpass',compact('hasil2'));
}

public function baronaskasihpass(Request $inputnya)
  {
    $IDupdate = $inputnya->input('updateID');
    $baru = User::find($IDupdate);
    $passwordcrypted = bcrypt($inputnya->input('password'));

    $baru->email = $inputnya->input('email');
    $baru->status = $inputnya->input('status');
    $baru->cadangan = $inputnya->input('password');
    $baru->password = $passwordcrypted;
    $baru->no_peserta = $inputnya->input('no_peserta');
    $baru->save();
  return redirect()->back()->with('message', 'Berhasil Tambah');
  }

  public function updatecheklist(Request $inputnya)
  {
    $IDupdate = $inputnya->input('updateID');
    $baru = User::find($IDupdate);
    $baru->kwitansi = $inputnya->input('kwitansi');
    $baru->b_pendaftaran = $inputnya->input('b_pendaftaran');

    $baru->save();
    return redirect()->back();
  }

}
