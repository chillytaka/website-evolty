<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;

class baronas extends Controller
{



    public function daftar(Request $inputnya)
      {

        $validator = Validator::make($inputnya->all(), [
          'name' => 'required|string|max:255',
          'email' => 'required|string|email|max:255|unique:users',

          'event' => 'required|string|max:255',
          'cp' => 'required|string|max:255',
          'kategory' => 'required|string|max:255',
          'bukti' => 'required|mimes:jpg,jpeg,png|max:2048',

        ])->validate();;

        


  		$fileNamebukti = 'null';
          $destinationPath = 'nope/gambar/buktibaronas';
          $extension = Input::file('bukti')->getClientOriginalExtension();
          $fileNamebukti = uniqid().'.'.$extension;
          Input::file('bukti')->move($destinationPath, $fileNamebukti);

          User::insert([
              [
                  'kategory' => $inputnya->input('kategory'),
                  'email' => $inputnya->input('email'),
                  'name' => $inputnya->input('name'),
        	        'notelp' => $inputnya->input('cp'),
    		        'bukti' => $fileNamebukti,
    		        'event' => $inputnya->input('event')
    	         ]
          ]);
          return redirect('/register/baronas/success');
      }



    }
