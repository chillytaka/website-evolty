<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;

class peserta extends Controller
{
    public function index()
    {
        $user = Auth::user();
        return view('editteam', compact('user'));
    }
    public function indexbaronas()
    {
        $user = Auth::user();
        return view('editteambaronas', compact('user'));
    }
    public function edit(Request $inputnya)
    {

      $baru = Auth::user();


      $baru->name = $inputnya->input('name');
      $baru->email = $inputnya->input('email');

      $baru->notelp = $inputnya->input('notelp');
      $baru->asalsekolah = $inputnya->input('asalsekolah');
      $baru->alamatsekolah = $inputnya->input('alamatsekolah');
      $baru->namaketua = $inputnya->input('namaketua');
      $baru->kelasketua = $inputnya->input('kelasketua');
      $baru->namaanggota1 = $inputnya->input('namaanggota1');
      $baru->kelasanggota1 = $inputnya->input('kelasanggota1');

      $baru->save();
    return redirect()->back()->with('message', 'Berhasil Edit');
    }

    public function baronasinput(Request $inputnya)
      {
        $fileNamekartupelajar1 = 'null';
        $destinationPath1 = 'nope/gambar/b_kartupelajar';
        if (Input::file('b_ktppeserta1') != null) {
          $extension = Input::file('b_ktppeserta1')->getClientOriginalExtension();
          $fileNamekartupelajar1 = uniqid().'.'.$extension;
          Input::file('b_ktppeserta1')->move($destinationPath1, $fileNamekartupelajar1);
        }


        $fileNamekartupelajar2 = 'null';
        $destinationPath2 = 'nope/gambar/b_kartupelajar';
        if (Input::file('b_ktppeserta2') != null) {
          $extension = Input::file('b_ktppeserta2')->getClientOriginalExtension();
          $fileNamekartupelajar2 = uniqid().'.'.$extension;
          Input::file('b_ktppeserta2')->move($destinationPath2, $fileNamekartupelajar2);
        }


        $fileNamekartupelajar3 = 'null';
        $destinationPath3 = 'nope/gambar/b_kartupelajar';
        if (Input::file('b_ktppeserta3') != null) {
          $extension = Input::file('b_ktppeserta3')->getClientOriginalExtension();
          $fileNamekartupelajar3 = uniqid().'.'.$extension;
          Input::file('b_ktppeserta3')->move($destinationPath3, $fileNamekartupelajar3);
        }


        $fileNamefoto1 = 'null';
        $destinationPath4 = 'nope/gambar/b_foto';
        if (Input::file('b_fotopeserta1') != null) {
          $extension = Input::file('b_fotopeserta1')->getClientOriginalExtension();
          $fileNamefoto1 = uniqid().'.'.$extension;
          Input::file('b_fotopeserta1')->move($destinationPath4, $fileNamefoto1);
        }


        $fileNamefoto2 = 'null';
        $destinationPath5 = 'nope/gambar/b_foto';
        if (Input::file('b_fotopeserta2') != null) {
          $extension = Input::file('b_fotopeserta2')->getClientOriginalExtension();
          $fileNamefoto2 = uniqid().'.'.$extension;
          Input::file('b_fotopeserta2')->move($destinationPath5, $fileNamefoto2);
        }


        $fileNamefoto3 = 'null';
        $destinationPath6 = 'nope/gambar/b_foto';
        if (Input::file('b_fotopeserta3') != null) {
          $extension = Input::file('b_fotopeserta3')->getClientOriginalExtension();
          $fileNamefoto3 = uniqid().'.'.$extension;
          Input::file('b_fotopeserta3')->move($destinationPath6, $fileNamefoto3);
        }


        $baru = Auth::user();

          $baru->b_namapembimbing = $inputnya->input('b_namapembimbing');
          $baru->b_namapeserta1 = $inputnya->input('b_namapeserta1');
          if($fileNamekartupelajar1 != 'null'){
            $baru->b_ktppeserta1 = $fileNamekartupelajar1;
          }
          if ($fileNamefoto1 != 'null'){
              $baru->b_fotopeserta1 = $fileNamefoto1;
          }

          $baru->b_namapeserta2 = $inputnya->input('b_namapeserta2');
          if($fileNamekartupelajar2 != 'null'){
            $baru->b_ktppeserta2 = $fileNamekartupelajar2;
          }
          if ($fileNamefoto2 != 'null'){
              $baru->b_fotopeserta2 = $fileNamefoto2;
          }
          $baru->b_namapeserta3 = $inputnya->input('b_namapeserta3');
          if($fileNamekartupelajar3 != 'null'){
            $baru->b_ktppeserta3 = $fileNamekartupelajar3;
          }
          if ($fileNamefoto3 != 'null'){
              $baru->b_fotopeserta3 = $fileNamefoto3;
          }
          $baru->asalsekolah = $inputnya->input('asalsekolah');
          $baru->alamatsekolah = $inputnya->input('alamatsekolah');
          $baru->email = $inputnya->input('email');
          $baru->save();
        return redirect('/home')->with('message', 'Berhasil Input');
      }


}
