<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
          'name' => 'string|max:255',
          'email' => 'required|string|email|max:255|unique:users',
          'password' => 'required|string|min:6|confirmed',
          'asalsekolah' => 'required|string|max:255',
          'alamatsekolah' => 'required|string|max:255',
          'namaketua' => 'required|string|max:255',
          'kelasketua' => 'required|string|max:3',
          // 'namaanggota1' => 'nullable|string|max:255',
          // 'kelasanggota1' => 'nullable|string|max:3',
          // 'namaanggota2' => 'nullable|string|max:255',
          // 'kelasanggota2' => 'nullable|string|max:3',
          'event' => 'required|string|max:255',
          'notelp' => 'required|string|max:255',
          'bukti' => 'required|mimes:jpg,jpeg,png|max:2048',
          'region' => 'required|string|max:25',
          'tipetes' => 'required|string|max:25',
          'tipedaftar' => 'required|string|max:15',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
              $fileName = 'null';

              $destinationPath = 'nope/gambar/rekap';
              $extension = Input::file('bukti')->getClientOriginalExtension();
              $fileName = uniqid().'.'.$extension;
              Input::file('bukti')->move($destinationPath, $fileName);

          return User::create([
              'name' => $data['name'],
              'email' => $data['email'],
              'password' => Hash::make($data['password']),
              'cadangan' => $data['password'],
              'asalsekolah' => $data['asalsekolah'],
              'alamatsekolah' => $data['alamatsekolah'],
              'namaketua' => $data['namaketua'],
              'kelasketua' => $data['kelasketua'],
              'namaanggota1' => $data['namaanggota1'],
              'kelasanggota1' => $data['kelasanggota1'],
              'event' => $data['event'],
              'notelp' => $data['notelp'],
              'bukti' => $fileName,
              'region' => $data['region'],
              'tipetes' => $data['tipetes'],
              'tipedaftar' => $data['tipedaftar'],
          ]);



    }
}
