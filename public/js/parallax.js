(function($){
    var multiply = 0.2;
    var $doc = $(document);
    $('.background').css({'background-position': '75% 50px' });
    $(window).scroll(function(){
        var top = $doc.scrollTop(),
            bg_pos = `75% ${multiply * -top+50}px`;
        $('.background').css({
            'background-position': bg_pos
        });
    });
})(jQuery)

new fullpage('#fullpage', {
    sectionsColor: ['#464b64', '#337d80', '#474761', '#3d6271'],
    navigation: true,
    slidesNavigation: true,
});