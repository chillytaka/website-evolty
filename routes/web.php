<?php

Route::view('/','welcome'); //mainpage
Route::view('/baronas', 'baronas');


//register
Route::view('/register/electra','daftarelectratutup');
Route::view('/register/ns','daftarns');
Route::view('/register/baronas','daftarbaronas');
Route::view('/register/baronas/success','daftarbaronasberhasil');
Route::view('/register/baronascoba','daftarbaronascoba');
Route::post('/registerbaronas','baronas@daftar');

//guidebook
Route::get('/gbelectra',function() {return response()->file('assets/pdf/gbelectra.pdf');});
Route::get('/hasilSMP', function() {return response()->file('assets/pdf/hasilkualifikasiSMP.pdf');});
Route::get('/gbbaronas',function() {return response()->file('assets/pdf/gbbaronas.pdf');});

//testing
Route::view('/ditotest', 'cobabaronas');
// Route::view('/coba','layouts.pageadmin');

//exportCSV
Route::view('/exportelectra','exportcsv');

//admin
Auth::routes();
Route::get('/adminelectra','admin@index')->middleware('auth','admin');
Route::get('/adminbaronas','admin@indexbaronas')->middleware('auth','admin');
Route::post('/adminelectra/update','admin@update');
Route::post('/adminelectra/delete','admin@delete');
Route::post('/adminelectra/verif','admin@verifikasi');
Route::post('/peserta_e_tambah','admin@tambah');
Route::view('/tambahpeserta_e','electratambah')->middleware('auth','admin');
Route::get('/search','admin@search')->middleware('auth','admin');
Route::get('/pesertaneedverif','admin@needverif')->middleware('auth','admin');
Route::get('/pesertaneedpass','admin@needpass')->middleware('auth','admin');
Route::post('/adminbaronas/kasihpass','admin@baronaskasihpass')->middleware('auth','admin');
Route::post('/updatecheklist','admin@updatecheklist')->middleware('auth','admin');

//Home, Edit team
Route::get('/home/editteam','peserta@index')->middleware('auth');
Route::get('/home/editteambaronas','peserta@indexbaronas')->middleware('auth');
Route::post('/home/editteambaronas/input','peserta@baronasinput')->middleware('auth');
Route::post('/home/editteam/edit','peserta@edit')->middleware('auth');
Route::get('/home', 'HomeController@index')->name('home');


//custom Login
Route::post('/login/custom', [

  'uses' => 'logincontroller@login',
  'as' => 'login.custom'

]);


  Route::get('/home', function()
  {
    return view('home');
  })->name('home')->middleware('auth');;

  Route::get('/adminarea', function()
  {
    return view('adminarea');
  })->name('adminarea')->middleware('auth','admin');;
