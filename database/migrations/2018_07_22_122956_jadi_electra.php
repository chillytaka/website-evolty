<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class JadiElectra extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function($table) {
        $table->string('tipedaftar')->nullable()->after('status');
          $table->string('asalsekolah');
           $table->string('alamatsekolah');
           $table->string('namaketua');
           $table->string('kelasketua');
           $table->string('namaanggota1');
           $table->string('kelasanggota1');
           $table->string('namaanggota2');
           $table->string('kelasanggota2');
           $table->string('event');
           $table->string('notelp');
           $table->binary('bukti');
           $table->string('region');
           $table->string('tipetes');
           $table->string('no_peserta')->nullable()->after('status');
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

      Schema::table('users', function($table) {
        $table->dropColumn('no_peserta');
    });

    }
}
